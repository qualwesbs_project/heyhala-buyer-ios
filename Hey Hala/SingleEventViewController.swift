//
//  SingleEventViewController.swift
//  Hey Hala
//
//  Created by Sagar Pandit on 10/05/21.
//

import UIKit
import PINRemoteImage

class SingleEventViewController: UIViewController {
    
    //MARK; IBOutlets
    @IBOutlet weak var galleryCollection: UICollectionView!
    @IBOutlet weak var timingView: View!
    @IBOutlet weak var bookNowTitle: DesignableUILabel!
    @IBOutlet weak var logoImage: ImageView!
    @IBOutlet weak var eventName: DesignableUILabel!
    @IBOutlet weak var eventDescription: DesignableUILabel!
    @IBOutlet weak var eventRating: DesignableUILabel!
    @IBOutlet weak var eventReview: DesignableUILabel!
    @IBOutlet weak var eventDistance: DesignableUILabel!
    @IBOutlet weak var eventAddress: DesignableUILabel!
    @IBOutlet weak var eventStatus: DesignableUILabel!
    @IBOutlet weak var heartImage: UIImageView!
    
    
    
    var isEvent = true
    var selectedStore = GetStoreResponse()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.timingView.isHidden = isEvent ? false:true
        self.bookNowTitle.text = isEvent ?  "Book Now":"Talk to Seller"
        self.initializeView()
    }
    
    func initializeView(){
        self.logoImage.pin_setImage(from: URL(string: U_IMAGE_BASE + (self.selectedStore.image ?? self.selectedStore.logo ?? "")))
        self.eventName.text = self.selectedStore.name
        self.eventDescription.text = self.selectedStore.description
        self.eventRating.text = "\(self.selectedStore.review?.rating ?? 0)"
        self.eventReview.text = "\(self.selectedStore.review?.reviews ?? 0) \n reviews"
        self.eventDistance.text = "\(self.selectedStore.distance ?? "") kms"
        self.eventAddress.text = self.selectedStore.location ?? ""
        self.galleryCollection.reloadData()
        
    }
    
    //MARK: IBActions
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func bookNowAction(_ sender: Any) {
        if(self.isEvent){
            let myVC = self.storyboard?.instantiateViewController(withIdentifier: "OrderRequestViewController") as! OrderRequestViewController
            myVC.selectedProduct = self.selectedStore
            self.navigationController?.pushViewController(myVC, animated: true)
        }else {
            let myVC = self.storyboard?.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
            self.navigationController?.pushViewController(myVC, animated: true)
        }
    }
    
    @IBAction func heartAction(_ sender: Any) {
        var url = String()
        if(heartImage.image == #imageLiteral(resourceName: "heart_outline")){
            self.heartImage.image = #imageLiteral(resourceName: "like")
            url = U_BASE + U_MARK_BUSINESS_FAVOURITE
        }else {
            self.heartImage.image = #imageLiteral(resourceName: "heart_outline")
            url = U_BASE + U_UNMARK_BUSINESS_FAVOURITE
        }
        
        SessionManager.shared.methodForApiCalling(url: url + "\(self.selectedStore.id ?? "")" , method: self.heartImage.image == #imageLiteral(resourceName: "like") ? .get:.post, parameter: nil, objectClass: CheckFavourite.self, requestCode: url) { (response) in
            if(self.heartImage.image != #imageLiteral(resourceName: "heart_outline")){
                self.showMsg(controller: self, text: "Successfully marked as favourite")
            }else {
                self.showMsg(controller: self, text: "Removed from favourite")
            }
            ActivityIndicator.hide()
        }
    }
    
    
    @IBAction func reviewAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "RateReviewViewController") as! RateReviewViewController
        myVC.produtDetail = self.selectedStore
        self.navigationController?.pushViewController(myVC, animated: true)
    }
    
    
}

extension SingleEventViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.selectedStore.images?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageCollection", for: indexPath) as! ImageCollection
        let val = self.selectedStore.images?[indexPath.row]
        cell.imageView.pin_setImage(from: URL(string: U_IMAGE_BASE + (val?.image ?? "")))
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (collectionView.frame.width/3)-10, height: (collectionView.frame.width/3)-10)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "GalleryViewController") as! GalleryViewController
        for val in self.selectedStore.images!{
            myVC.imageData?.append(val.image ?? "")
        }
        
        self.navigationController?.pushViewController(myVC, animated: true)
    }
}

class ImageCollection: UICollectionViewCell {
    //MARK: IBOutlets
    @IBOutlet weak var imageView: ImageView!
    @IBOutlet weak var itemName: UILabel!
}
