//
//  ProfileDetailViewController.swift
//  Hey Hala
//
//  Created by Apple on 17/04/21.
//

import UIKit
import iOSDropDown
import PINRemoteImage

class ProfileDetailViewController: UIViewController, SelectDate {
    func selectedDate(timestamp: Date) {
        self.dateOfBirth.text = self.convertTimestampToDate(Int(timestamp.timeIntervalSince1970), to: "yyyy-MM-dd")
    }
    
    //MARK: IBOutlets
    @IBOutlet weak var userGender: DropDown!
    @IBOutlet weak var username: DesignableUITextField!
    @IBOutlet weak var dateOfBirth: DesignableUITextField!
    @IBOutlet weak var userImage: ImageView!
    @IBOutlet weak var emailAddress: DesignableUITextField!
    @IBOutlet weak var mobileNumber: DesignableUITextField!
    @IBOutlet weak var fullName: DesignableUILabel!
    @IBOutlet weak var userEmail: DesignableUILabel!
    @IBOutlet weak var saveButton: UIButton!
    
    
    var navigationFromVerification = false
    var imagePath  = ""
    var imageName = ""
    let picker = UIImagePickerController()
    var number: String?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.picker.delegate = self
        self.mobileNumber.text = number
        self.mobileNumber.delegate = self
        username.attributedPlaceholder = NSAttributedString(string: "Name",
                                                                      attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        dateOfBirth.attributedPlaceholder = NSAttributedString(string: "Date of birth",
                                                                      attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        userGender.attributedPlaceholder = NSAttributedString(string: "Gender",
                                                                      attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        mobileNumber.attributedPlaceholder = NSAttributedString(string: "Mobile Number",
                                                                      attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        emailAddress.attributedPlaceholder = NSAttributedString(string: "Email Address",
                                                                      attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        userGender.optionArray = ["Male", "Female"]
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.initUser()
    }
    
    func initUser(){
        self.fullName.text = Singleton.shared.userDetail.fullName
        self.username.text = Singleton.shared.userDetail.fullName
        self.userEmail.text = Singleton.shared.userDetail.email
        self.emailAddress.text = Singleton.shared.userDetail.email
        self.mobileNumber.text = Singleton.shared.userDetail.phone ?? self.number
        self.userGender.text = Singleton.shared.userDetail.gender
        self.dateOfBirth.text = Singleton.shared.userDetail.dob
        self.userImage.pin_setImage(from: URL(string: U_IMAGE_BASE + (Singleton.shared.userDetail.profile ?? "")))
    }
    
    ///MARK: IBActions
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func saveAction(_ sender: Any) {
        if(Singleton.shared.userDetail.id != nil){
           
        }else {
        if(username.text!.isEmpty){
            self.showMsg(controller: self, text: "Enter name")
        }else if(dateOfBirth.text!.isEmpty){
            self.showMsg(controller: self, text: "Select date of birth")
        }else if(userGender.text!.isEmpty){
            self.showMsg(controller: self, text: "Select gender")
        }else if(mobileNumber.text!.isEmpty){
            self.showMsg(controller: self, text: "Enter mobile number")
        } else if(emailAddress.text!.isEmpty){
            self.showMsg(controller: self, text: "Enter email address")
        }else if !(self.isValidEmail(emailStr:emailAddress.text ?? "")){
            self.showMsg(controller: self, text: "Enter valid email address")
        }else {
          //  ActivityIndicator.show(view: self.view)
            let param:[String:Any] = [
                "fullName":self.username.text ?? "",
                "dob":self.dateOfBirth.text ?? "",
                "gender": ((self.userGender.text ?? "") == "Male") ? 1:2,
                "phone":self.mobileNumber.text ?? "",
                "email":self.emailAddress.text ?? "",
                "profile": self.imagePath
            
            ]
            SessionManager.shared.methodForApiCalling(url:U_BASE + U_SIGN_UP, method: .post, parameter: param, objectClass: Login.self, requestCode: U_SIGN_UP) { (response) in
                //ActivityIndicator.hide()
                self.showMsg(controller: self, text:"Profile updated successfully")
                UserDefaults.standard.setValue(response.response.token ?? "", forKey: UD_TOKEN)
                Singleton.shared.userDetail = response.response
                Singleton.shared.saveUserInfo(user: response.response)
                if(self.navigationFromVerification){
                    let myVC = self.storyboard?.instantiateViewController(withIdentifier: "TabbarViewController") as! TabbarViewController
                    self.navigationController?.pushViewController(myVC, animated: true)
                }
            }
        }
        }
    }
    
    @IBAction func dobAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "DatePickerViewController") as! DatePickerViewController
        myVC.maxDate = Date()
        myVC.modalPresentationStyle = .overFullScreen
        myVC.dateDelegate = self
        self.navigationController?.present(myVC, animated: true, completion: nil)
    }
    
    @IBAction func changeImageAction(_ sender: Any) {
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            self.openCamera()
        }))
        
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            self.openGallary()
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        self.addActionSheetForiPad(actionSheet: alert)
        self.present(alert, animated: true, completion: nil)
    }
    
    
}

extension ProfileDetailViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextFieldDelegate  {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if(textField == mobileNumber){
            self.mobileNumber.text =  self.mobileNumber.text?.applyPatternOnNumbers(pattern: "(###) ###-####", replacmentCharacter: "#")
            let currentCharacterCount = self.mobileNumber.text?.count ?? 0
            if (range.length + range.location > currentCharacterCount){
                return false
            }
            let newLength = currentCharacterCount + string.count - range.length
            return newLength <= 14
        }
        return true
    }
    
    func imagePickerController(_ picker: UIImagePickerController,
                               didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]){
        guard let selectedImage = info[UIImagePickerController.InfoKey.editedImage] as? UIImage else { return }
        
        if let selectedImageName = ((info[UIImagePickerController.InfoKey.referenceURL] as? NSURL)?.lastPathComponent) {
            
            self.imageName = selectedImageName
        }else {
            self.imageName = "image.jpg"
        }
        let imageData:NSData = selectedImage.pngData()! as NSData
        
        //save in user default
        let strBase64 = imageData.base64EncodedString(options: .lineLength64Characters)
        if let cropImage = selectedImage as? UIImage {
            let data = cropImage.jpegData(compressionQuality: 0.2) as Data?
            let params = [
                "image": data!,
                "type": 1
                ] as [String : Any]
            // HTTP Request for upload Picture
            ActivityIndicator.show(view: self.view)
            
            SessionManager.shared.makeMultipartRequest(url: U_BASE + U_UPLOAD_IMAGE, fileData: data!, param: params, objectClass: UploadImage.self, fileName: imageName) { (response) in
                self.imagePath = response.response?.url ?? ""
                self.userImage.image = cropImage
                ActivityIndicator.hide()
                
            }
            
        }
        picker.dismiss(animated: true)
    }
    
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera))
        {
            picker.sourceType = UIImagePickerController.SourceType.camera
            picker.allowsEditing = true
            self.present(picker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func openGallary()
    {
        picker.sourceType = UIImagePickerController.SourceType.photoLibrary
        picker.allowsEditing = true
        self.present(picker, animated: true, completion: nil)
    }
}
