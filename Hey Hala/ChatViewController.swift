//
//  ChatViewController.swift
//  Hey Hala
//
//  Created by Sagar Pandit on 13/05/21.
//
import UIKit
import UserNotifications
//import SDWebImage
//import FirebaseStorage
//import Firebase

class ChatViewController: UIViewController {
    
    //MARK:IBOutlets
    @IBOutlet weak var chatTable: UITableView!
    @IBOutlet weak var userImage: ImageView!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var occupation: DesignableUILabel!
    
    
    var chatData = [ChatResponse]()
    var userId = Int()
    var driverId  = Int()
    var chatId = String()
    let picker = UIImagePickerController()
    var imageName = ""
    var imageData  = Data()
    var jobDetail : ChatDetail?
    var senderID = Int()
    var receiverID = Int()
    var senderName = String()
    var receiverName = String()
    var postedByMe = false
    var isAdminChat = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        picker.delegate = self
       
        //self.view.backgroundColor = darkBlue
//        if(isAdminChat){
//            userImage.image = #imageLiteral(resourceName: "CATLEY_LAKEMAN-Russell")
//            self.lblUserName.text =  "Admin"
//                    self.occupation.text = "Admin"
//            self.senderID =  jobDetail?.sender_id ?? 0
//            self.receiverID = jobDetail?.receiver_id ?? 0
//            self.senderName = jobDetail?.sender_name ?? ""
//            self.receiverName = self.jobDetail?.receiver_name ?? ""
//            self.chatId = "\(self.jobDetail?.sender_id ?? 0)" + "_" + "\(self.jobDetail?.receiver_id ?? 0)"
//            self.addChatObserver()
//        }else {
//        if(jobDetail?.shipment_id != nil){
//            if(Singleton.shared.userType.user_id == self.jobDetail?.sender_id){
//                userImage.sd_setImage(with:
//                              URL(string:self.jobDetail?.receiver_image ?? ""), placeholderImage: #imageLiteral(resourceName: "CATLEY_LAKEMAN-Russell"))
//                          self.lblUserName.text = jobDetail?.receiver_name ?? ""
//                        self.occupation.text = "Driver"
//                self.senderID =  jobDetail?.sender_id ?? 0
//                self.receiverID = jobDetail?.receiver_id ?? 0
//                self.senderName = jobDetail?.sender_name ?? ""
//                self.receiverName = self.jobDetail?.receiver_name ?? ""
//
//                 }else {
//                  userImage.sd_setImage(with:URL(string:self.jobDetail?.sender_image ?? ""), placeholderImage:#imageLiteral(resourceName: "CATLEY_LAKEMAN-Russell") )
//                                self.lblUserName.text = jobDetail?.sender_name ?? ""
//                                self.occupation.text = "Customer"
//                                self.senderID = jobDetail?.receiver_id ?? 0
//                                 self.receiverID = jobDetail?.sender_id ?? 0
//                                 self.senderName = jobDetail?.receiver_name ?? ""
//                                 self.receiverName = self.jobDetail?.sender_name ?? ""
//                }
//            self.chatId = "\(self.jobDetail?.sender_id ?? 0)" + "_" + "\(self.jobDetail?.shipment_id ?? 0)" + "_" + "\(self.jobDetail?.receiver_id ?? 0)"
//            self.addChatObserver()
//        }
//        }
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
     self.navigationController?.isNavigationBarHidden = false
     self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
   
    
//    func addChatObserver() {
//        ref.child("messages/" + "\(self.chatId)").observe(.childAdded, with: { (snapshot) in
//            if let myVal = snapshot.value as? [String:Any]{
//                self.chatData.append(ChatResponse(id: myVal["id"] as! String, message: myVal["message"] as! String, read_satus: myVal["read_status"] as! Int, receiver_id: myVal["receiver_id"] as! Int, sender_id: myVal["sender_id"] as! Int, time: myVal["time"] as! Int, type: myVal["type"] as! Int))
//
//                self.chatTable.beginUpdates()
//                self.chatTable.insertRows(at: [IndexPath(row: self.chatData.count - 1, section: 0)], with: .bottom)
//                self.chatTable.endUpdates()
//                self.chatTable.scrollToRow(at: IndexPath(row: self.chatData.count - 1, section: 0), at: .bottom, animated: false)
//            }
//        })
//    }
    
//    func uplaodUserImage() {
//        ActivityIndicator.show(view: self.view)
//        let fileData = self.imageData
//        let storeImg = storageRef.child("chat_photos/" + "\(self.chatId)").child(self.imageName)
//        storeImg.putData(fileData, metadata:StorageMetadata(dictionary: ["contentType": "image/png"])) { (snapshot, error) in
//            // When the image has successfully uploaded, we get it's download URL
//            self.imageData = Data()
//            storeImg.downloadURL(completion: { (url, error) in
//                guard let downloadURL = url else {
//                    return
//                }
//                let time =  Int(Date().timeIntervalSince1970)
//                //let reqId =  self.chatDetail.j ?? 0
//                // Write the download URL to the Realtime Database
//                let dbRef = ref.child("messages/" + self.chatId).childByAutoId().setValue([
//                    "id": self.chatId,
//                    "time": time,
//                    //"request_id": reqId,
//                    "sender_id": self.senderID,
//                    "receiver_id": self.receiverID,
//                    "message": downloadURL.absoluteString,
//                    "type": 2,
//                    "read_status": 1,
//                ])
////                var curUser = String()
////                if(K_CURRENT_USER == K_POST_JOB){
////                    curUser = K_WANT_JOB
////                }else{
////                    curUser = K_POST_JOB
////                }
////                self.sendSingleMessage(type: 2, recieverId: self.senderID, message: downloadURL.absoluteString, senderId: self.receiverID, senderType: K_CURRENT_USER ?? "" , receiverType: curUser, messageType: 2)
//                ActivityIndicator.hide()
//            })
//        }
//    }
    
    func scheduleNotifications(title:String,msg: String) {
        let notificationContent = UNMutableNotificationContent()
        notificationContent.title = title
        notificationContent.body = msg
        notificationContent.sound = UNNotificationSound.default
        
        // Deliver the notification in five seconds.
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 0.01, repeats: false)
        // Schedule the notification.
        let request = UNNotificationRequest(identifier: "FiveSecond", content: notificationContent, trigger: trigger)
        let center = UNUserNotificationCenter.current()
        center.add(request) { (error: Error?) in
            if let theError = error {
                print(theError)
            }
        }
    }
    
//    func sendSingleMessage(type: Int, recieverId: String, message: String,senderId: String, senderType: String, receiverType: String, messageType:Int) {
//            let param = [
//             "sender":senderId,
//             "receiver":recieverId,
//             "sender_type":senderType,
//             "receiver_type":receiverType,
//             "message_type": messageType,
//             "last_message":message,
//             "last_message_by":senderId,
//             "job_id":self.jobDetail?.id
//            ] as? [String:Any]
//
//            SessionManager.shared.methodForApiCalling(url: U_BASE + U_SEND_CHAT_MESSAGE, method: .post, parameter: param, objectClass: Response.self, requestCode: U_SEND_CHAT_MESSAGE) { (response) in
//
//            }
//        }
    
    
    //MARK: IBAction
    @IBAction func sendAction(_ sender: Any) {
        if !(self.textView.text!.isEmpty && self.jobDetail?.shipment_id != nil){
            self.textView.resignFirstResponder()
            let id = self.chatId
            let time =  Int(Date().timeIntervalSince1970)
            self.textView.text = ""
            self.chatTable.beginUpdates()
            self.chatTable.endUpdates()
        }
    }
    
    
    @IBAction func attachmentAction(_ sender: Any) {
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            self.openCamera()
        }))
        
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            self.openGallary()
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        self.addActionSheetForiPad(actionSheet: alert)
        self.present(alert, animated: true, completion: nil)
    }
    
    
    
    @IBAction func backBtnPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}

extension ChatViewController: UIImagePickerControllerDelegate,UINavigationControllerDelegate  {


    func imagePickerController(_ picker: UIImagePickerController,
                               didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]){
        guard let selectedImage = info[UIImagePickerController.InfoKey.editedImage] as? UIImage else {
            self.dismiss(animated: true, completion: nil)
            return

        }

        if let selectedImageName = ((info[UIImagePickerController.InfoKey.referenceURL] as? NSURL)?.lastPathComponent) {
            self.imageName = selectedImageName
            //            self.addVehicle.setTitle("", for: .normal)
        }else {
            self.imageName = "image.jpg"
        }
        self.imageData = selectedImage.pngData()!
      //  self.uplaodUserImage()
        self.dismiss(animated: true, completion: nil)
    }

    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera))
        {
            picker.sourceType = UIImagePickerController.SourceType.camera
            picker.allowsEditing = true
            self.present(picker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }

    func openGallary()
    {
        picker.sourceType = UIImagePickerController.SourceType.photoLibrary
        picker.allowsEditing = true
        self.present(picker, animated: true, completion: nil)
    }

}



class ChatViewCell: UITableViewCell {
    //MARK:IBOutlets
    @IBOutlet weak var messageTime: UILabel!
    @IBOutlet weak var messageText: UILabel!
    @IBOutlet weak var uploadedImage: UIImageView!
    @IBOutlet weak var messageTextView: UIView!
    
}

