//
//  TransactionViewController.swift
//  Hey Hala
//
//  Created by Apple on 17/04/21.
//

import UIKit
import iOSDropDown

class TransactionViewController: UIViewController {
    //MARK: IBOutlets
    @IBOutlet weak var sortBy: DropDown!
    @IBOutlet weak var transactionView: UIView!
    @IBOutlet weak var titleLabel: DesignableUILabel!
    @IBOutlet weak var titleDescription: DesignableUILabel!
    @IBOutlet weak var tableView: UITableView!
    
    let sortOption = ["Recent", "Newest", "Oldest", "Date (From To)"]
    var isEventRequest =  false
    var serviceData = [ServiceRequestResponse]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.sortBy.optionArray = sortOption
        self.transactionView.isHidden = isEventRequest
        self.titleLabel.text = isEventRequest ? "Event Request":"Transactions"
        self.titleDescription.text = isEventRequest ? "Requests on event will appear here":"Transactions will appear here"
        if(self.isEventRequest){
            ActivityIndicator.show(view: self.view)
            SessionManager.shared.methodForApiCalling(url: U_BASE + U_GET_REQUEST_SERVICE, method: .get, parameter: nil, objectClass: GetServiceRequest.self, requestCode: U_GET_REQUEST_SERVICE) { (response) in
                self.serviceData = response.response
                self.tableView.reloadData()
                ActivityIndicator.hide()
            }
        }else {
            
        }
    }
    
    //MARK: IBActions
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}

extension TransactionViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.serviceData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TransactionCell") as! TransactionCell
        let val = self.serviceData[indexPath.row]
        if(self.isEventRequest){
            cell.dateLabel.isHidden = true
            cell.nameLabel.text = val.description
            cell.descriptionLabel.text = self.convertTimestampToDate(val.date ?? 0, to: "dd/MM/yyyy")
            if(val.status?.status == "1"){
                cell.status.text = "Pending ⌛"
            }else if(val.status?.status == "2"){
                cell.status.text = "Seller accepted"
            }else if(val.status?.status == "3"){
                cell.status.text = "Rejected 🔴"
            }else if(val.status?.status == "4"){
                cell.status.text = "Accepted 🟢"
            }
        }else {
            
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(isEventRequest){
            let myVC = self.storyboard?.instantiateViewController(withIdentifier: "OrderRequestViewController") as! OrderRequestViewController
            myVC.isEventRequest = true
            myVC.serviceRequestData = self.serviceData[indexPath.row]
            self.navigationController?.pushViewController(myVC, animated: true)
        }
    }
    
}

class TransactionCell: UITableViewCell {
    //MARK: IBOutlets
    
    @IBOutlet weak var descriptionLabel: DesignableUILabel!
    @IBOutlet weak var nameLabel: DesignableUILabel!
    @IBOutlet weak var status: DesignableUILabel!
    @IBOutlet weak var dateLabel: DesignableUILabel!
}
