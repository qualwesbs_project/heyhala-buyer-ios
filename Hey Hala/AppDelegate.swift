//
//  AppDelegate.swift
//  Hey Hala
//
//  Created by Apple on 25/03/21.
//

import UIKit
import GooglePlaces
import GoogleMaps
import CoreLocation
import IQKeyboardManager


@main
class AppDelegate: UIResponder, UIApplicationDelegate, CLLocationManagerDelegate {
    
    var window: UIWindow?
    lazy var locationManager = CLLocationManager()
    
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        self.window?.backgroundColor = .white
        self.locationManager.delegate = self
        IQKeyboardManager.shared().isEnabled = true
        IQKeyboardManager.shared().isEnableAutoToolbar = false
        // Override point for customization after application launch.
        self.initializeGoogleMap()
        
        return true
    }
    
    func initializeGoogleMap(){
        GMSPlacesClient.provideAPIKey("AIzaSyBvOO-m6sV_jFWGUWIx5aPqBGfgI8d3HeI")
        GMSServices.provideAPIKey("AIzaSyBvOO-m6sV_jFWGUWIx5aPqBGfgI8d3HeI")
        self.handleUserLocation()
        //   GIDSignIn.sharedInstance().clientID = FirebaseApp.app()?.options.clientID
    }
    
    func handleUserLocation(){
        switch CLLocationManager.authorizationStatus() {
        case .authorizedAlways, .authorizedWhenInUse:
            locationManager.delegate = self
            if let loc = locationManager.location {
                Singleton.shared.userLocation = loc.coordinate
            }
        case .notDetermined:
            locationManager.delegate = self
            locationManager.requestWhenInUseAuthorization()
        case .denied:
            self.createSettingsAlertController(title: "Enable Location", message: "This app requires to get current location of user for better experience.")
        case .restricted:
            // Nothing you can do, app cannot use location services
            break
        }
    }
    
    func createSettingsAlertController(title: String, message: String) {
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let cancelAction = UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel){ (UIAlertAction) in
            exit(0)
        }
        let settingsAction = UIAlertAction(title: NSLocalizedString("Settings", comment: ""), style: .default) { (UIAlertAction) in
            UIApplication.shared.open(URL(string:UIApplication.openSettingsURLString)!)
        }
        
        alertController.addAction(cancelAction)
        alertController.addAction(settingsAction)
        self.window?.rootViewController?.present(alertController, animated: true, completion: nil)
        
    }
    
        func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
            if(locations.count > 0){
                //Singleton.shared.userLocation = locations[0]
            }
        }
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .notDetermined:
            manager.requestLocation()
        case .authorizedAlways, .authorizedWhenInUse:
            locationManager.delegate = self
            if let loc = locationManager.location {
                Singleton.shared.userLocation = loc.coordinate
            }
            break
        case .denied:
            self.createSettingsAlertController(title: "Enable Location", message: "This app requires to get current location of user for better experience.")
        default:
            
            break
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(error)
    }
}

