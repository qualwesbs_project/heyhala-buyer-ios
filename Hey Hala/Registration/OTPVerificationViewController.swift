//
//  OTPVerificationViewController.swift
//  Hey Hala
//
//  Created by Apple on 02/04/21.
//

import UIKit
class OtpVerificationViewController: UIViewController {
    //MARK: IBOutlets
    @IBOutlet weak var heyhalaLabel: DesignableUILabel!
    @IBOutlet weak var otpLabel: DesignableUILabel!
    @IBOutlet weak var firstField: DesignableUITextField!
    @IBOutlet weak var secondField: DesignableUITextField!
    @IBOutlet weak var thirdField: DesignableUITextField!
    @IBOutlet weak var fourthField: DesignableUITextField!
    
    var number = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.firstField.delegate = self
        self.secondField.delegate = self
        self.thirdField.delegate = self
        self.fourthField.delegate = self
        self.firstField.becomeFirstResponder()
        self.getOtp()

    }
    
    func getOtp() {
        ActivityIndicator.show(view: self.view)
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_GET_OTP + "\(self.number)", method: .get, parameter: nil, objectClass: GetOtp.self, requestCode: U_GET_OTP) { (response) in
            self.showMsg(controller: self, text: "Your OTP is \(response.response?.otp ?? 0)")
            ActivityIndicator.hide()
        }
    }
    
    //MARK: IBActions

    @IBAction func confirmAction(_ sender: Any) {
        ActivityIndicator.show(view: self.view)
        var otp = (self.firstField.text ?? "") + (self.secondField.text ?? "") + (self.thirdField.text ?? "")
        otp = otp + (self.fourthField.text ?? "")
        let param:[String:Any] = [
            "phone": self.number,
            "otp": Int(otp) ?? 0,
            "userType":2,
        ]
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_VERIFY_OTP, method: .post, parameter: param, objectClass: Login.self, requestCode: U_VERIFY_OTP) { (response) in
            if(response.response.id == nil){
                self.showMsg(controller: self, text:"Number verified, please complete your profile")
                let myVC = self.storyboard?.instantiateViewController(withIdentifier: "ProfileDetailViewController") as! ProfileDetailViewController
                myVC.navigationFromVerification = true
                myVC.number = self.number
                self.navigationController?.pushViewController(myVC, animated: true)
            }else {
                self.showMsg(controller: self, text:"Logged in successfully")
                UserDefaults.standard.setValue(response.response.token ?? "", forKey: UD_TOKEN)
                Singleton.shared.userDetail = response.response
                Singleton.shared.saveUserInfo(user: response.response)
            let myVC = self.storyboard?.instantiateViewController(withIdentifier: "TabbarViewController") as! TabbarViewController
            self.navigationController?.pushViewController(myVC, animated: true)
            }
            ActivityIndicator.hide()
        }
    }
    
    @IBAction func resendAction(_ sender: Any) {
        self.getOtp()
    }
    
}

extension OtpVerificationViewController: UITextFieldDelegate{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let  char = string.cString(using: String.Encoding.utf8)!
        let isBackSpace = strcmp(char, "\\b")
        if(textField == firstField){
            if(textField.text!.count == 0){
                textField.text = string
                self.secondField.becomeFirstResponder()
                return true
            }else if (isBackSpace == -92) {
                textField.text = ""
            }
        }else if(textField == secondField){
            if(textField.text!.count == 0){
                textField.text = string
                self.thirdField.becomeFirstResponder()
                return true
                
            }else if (isBackSpace == -92) {
                textField.text = ""
                self.firstField.becomeFirstResponder()
            }
        }else if(textField == thirdField){
            if(textField.text!.count == 0){
                textField.text = string
                self.fourthField.becomeFirstResponder()
                return true
            }else if (isBackSpace == -92) {
                textField.text = ""
                self.secondField.becomeFirstResponder()
            }
        }else if(textField == fourthField){
            if(textField.text!.count == 0){
                textField.text = string
                self.fourthField.resignFirstResponder()
                return true
            }else if (isBackSpace == -92) {
                textField.text = ""
                self.thirdField.becomeFirstResponder()
            }
        }
        return false
    }
}
