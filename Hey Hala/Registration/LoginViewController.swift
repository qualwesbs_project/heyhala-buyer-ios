//
//  LoginViewController.swift
//  Hey Hala
//
//  Created by Apple on 02/04/21.
//

import UIKit


class LoginViewController: UIViewController, UITextFieldDelegate {
    //MARK: IBOutlets
    @IBOutlet weak var appName: DesignableUILabel!
    @IBOutlet weak var loginLabel: DesignableUILabel!
    @IBOutlet weak var welcomeBackLabel: DesignableUILabel!
    @IBOutlet weak var countryCodeField: DesignableUITextField!
    @IBOutlet weak var contactNumberField: DesignableUITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        countryCodeField.attributedPlaceholder = NSAttributedString(string: "+966",
                                                                    attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        contactNumberField.attributedPlaceholder = NSAttributedString(string: "Register mobile number",
                                                                      attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        self.contactNumberField.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.contactNumberField.text = ""
        self.countryCodeField.text = ""
    }
    
    //MARK: IBActions
    @IBAction func loginAction(_ sender: Any) {
        if(self.contactNumberField.text!.isEmpty){
            self.showMsg(controller: self, text: "Enter contact number")
        }else {
            var number = self.contactNumberField.text?.replacingOccurrences(of: "(", with: "")
            number = number?.replacingOccurrences(of: ")", with: "")
            number = number?.replacingOccurrences(of: "-", with: "")
            number = number?.replacingOccurrences(of: " ", with: "")
          let myVC = self.storyboard?.instantiateViewController(withIdentifier: "OtpVerificationViewController") as! OtpVerificationViewController
            myVC.number = number ?? ""
          self.navigationController?.pushViewController(myVC, animated: true)
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if(textField == contactNumberField){
            self.contactNumberField.text =  self.contactNumberField.text?.applyPatternOnNumbers(pattern: "(###) ###-####", replacmentCharacter: "#")
            let currentCharacterCount = self.contactNumberField.text?.count ?? 0
            if (range.length + range.location > currentCharacterCount){
                return false
            }
            let newLength = currentCharacterCount + string.count - range.length
            return newLength <= 14
        }
        return true
    }
}

