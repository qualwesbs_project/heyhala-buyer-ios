//
//  ProfileViewController.swift
//  Hey Hala
//
//  Created by Apple on 31/03/21.
//

import UIKit
import RAMAnimatedTabBarController


class ProfileViewController: UIViewController {
    //MARK: IBOutlets
    @IBOutlet weak var profileTab: RAMAnimatedTabBarItem!
    @IBOutlet weak var fullName: DesignableUILabel!
    @IBOutlet weak var userImage: ImageView!
    @IBOutlet weak var userEmail: DesignableUILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.profileTab.animation.iconSelectedColor = .white
        self.profileTab.animation.textSelectedColor = .white
        profileTab.playAnimation()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.fullName.text = Singleton.shared.userDetail.fullName
        self.userEmail.text = Singleton.shared.userDetail.email
        self.userImage.pin_setImage(from: URL(string: U_IMAGE_BASE + (Singleton.shared.userDetail.profile ?? "")))
    }
    
    
    //MARK: IBAction
    @IBAction func profileAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "ProfileDetailViewController") as! ProfileDetailViewController
        self.navigationController?.pushViewController(myVC, animated: true)
    }
    
    @IBAction func addressAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "AddAddressViewController") as! AddAddressViewController
        self.navigationController?.pushViewController(myVC, animated: true)
    }
    
    
    @IBAction func transactionAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "TransactionViewController") as! TransactionViewController
        self.navigationController?.pushViewController(myVC, animated: true)
    }
    
    
    @IBAction func accountSetting(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "AccountSettingViewController") as! AccountSettingViewController
        self.navigationController?.pushViewController(myVC, animated: true)
    }
    
    @IBAction func orderHistoryAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "MyOrdersViewController") as! MyOrdersViewController
        myVC.showBackButton = true
        self.navigationController?.pushViewController(myVC, animated: true)
    }
    
    @IBAction func saveWhishlistAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "SavedWishlistViewController") as! SavedWishlistViewController
        self.navigationController?.pushViewController(myVC, animated: true)
    }
    
    @IBAction func eventAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "TransactionViewController") as! TransactionViewController
        myVC.isEventRequest = true
        self.navigationController?.pushViewController(myVC, animated: true)
    }
    
    @IBAction func logoutAction(_ sender: Any) {
        Singleton.shared.initialiseValues()
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        self.navigationController?.pushViewController(myVC, animated: true)
    }
}

