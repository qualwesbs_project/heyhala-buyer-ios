//
//  SavedWishlistViewController.swift
//  Hey Hala
//
//  Created by Apple on 19/04/21.
//

import UIKit

class SavedWishlistViewController: UIViewController {
    //MARK: IBOUtlets
    @IBOutlet weak var favouriteTable: UITableView!
    @IBOutlet weak var noWishlistLabel: DesignableUILabel!
    
    var productData = [GetWishListResponse]()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.getWishListItems()
    }
    
    func getWishListItems(){
        ActivityIndicator.show(view: self.view)
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_GET_SAVED_WISHLIST, method: .get, parameter: nil, objectClass: GetWishlist.self, requestCode: U_GET_SAVED_WISHLIST) { (response) in
            self.productData = response.response
            ActivityIndicator.hide()
            self.favouriteTable.reloadData()
        }
    }
    
    //MARK: IBAction
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}


extension SavedWishlistViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(self.productData.count == 0){
            self.noWishlistLabel.isHidden = false
        }else {
            self.noWishlistLabel.isHidden = true
        }
        return self.productData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let val = self.productData[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "ItemTablecell") as! ItemTablecell
        cell.itemImage.pin_setImage(from: URL(string: U_IMAGE_BASE + (val.product?.image ?? "") ))
        cell.itemDescription.text = val.product?.description
        cell.itemName.text = val.product?.name
        cell.itemStatus.text = "\(val.product?.preprationTime ?? 0) min"
        cell.itemRating.text =  "\(val.product?.review?.rating ?? 0)"
        cell.distance.text = "\(val.product?.distance ?? "0") kms"
        cell.itemPrice.text = "SAR \(val.product?.price ?? 0).00"
        cell.deleteButton={
            ActivityIndicator.show(view: self.view)

            SessionManager.shared.methodForApiCalling(url: U_BASE + U_DELETE_WISHLIST_PRODUCT, method: .post, parameter: ["product":val.product?.id ?? ""], objectClass: SuccessResponse.self, requestCode: U_DELETE_WISHLIST_PRODUCT) { (response) in
                ActivityIndicator.hide()
                self.showMsg(controller: self, text: "Successfully removed from wishlist")
                self.getWishListItems()
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "OrderNowViewController") as! OrderNowViewController
        self.navigationController?.pushViewController(myVC, animated: true)
    }
}
