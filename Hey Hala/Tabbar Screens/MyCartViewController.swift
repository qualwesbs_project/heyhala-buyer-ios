//
//  MyCartViewController.swift
//  Hey Hala
//
//  Created by Apple on 31/03/21.
//

import UIKit
import RAMAnimatedTabBarController
import Cosmos
import PINRemoteImage

class MyCartViewController: UIViewController, SlideButtonDelegate{
    
    
    func buttonStatus(status: String, sender: MMSlidingButton) {
        if(status == "Unlocked"){
            if(self.selectedAddress.id == nil){
                self.showMsg(controller: self, text: "Select address")
                self.slideButtonView.reset()
            }else if(self.cartData.items.count == 0){
                self.showMsg(controller: self, text: "Please add some item to proceed")
                self.slideButtonView.reset()
            }else {
                let myVC = self.storyboard?.instantiateViewController(withIdentifier: "CheckoutViewController") as! CheckoutViewController
                myVC.selectedAddress = self.selectedAddress 
                self.navigationController?.pushViewController(myVC, animated: true)
            }
        }
    }
    
    
    //MARK: IBOutlets
    @IBOutlet weak var addressTable: ContentSizedTableView!
    @IBOutlet weak var itemTable: ContentSizedTableView!
    @IBOutlet weak var slideButtonView: MMSlidingButton!
    @IBOutlet weak var cartTab: RAMAnimatedTabBarItem!
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var itemTotal: DesignableUILabel!
    @IBOutlet weak var taxRate: DesignableUILabel!
    @IBOutlet weak var deliveryFee: DesignableUILabel!
    @IBOutlet weak var cartTotal: DesignableUILabel!
    @IBOutlet weak var deliveryDate: DesignableUILabel!
    
    
    
    var showBackButton = false
    var addressData = [AddressResponse]()
    var selectedAddress = AddressResponse()
    var cartData = GetCartResponse()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.slideButtonView.delegate = self
        if(self.showBackButton){
            self.backView.isHidden = false
        }
        self.addressTable.reloadData()
        self.itemTable.reloadData()
        self.cartTab.animation.iconSelectedColor = .white
        self.cartTab.animation.textSelectedColor = .white
        cartTab.playAnimation()
        self.deliveryDate.text = self.convertTimestampToDate(Int(Date().timeIntervalSince1970), to: "MMM dd, yyyy")
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        NavigationController.shared.getAllAddress(view: self) { (response) in
            
            
            if(response.count > 0){
                self.addressData = response.filter{
                    $0.is_default_address == 1
                }
                if(self.addressData.count == 0){
                    self.addressData.append(response[0])
                }
                self.addressTable.reloadData()
            }
            
        }
        self.callCartAPI()
    }
    
    func callCartAPI(){
        NavigationController.shared.getcartData(view: self) { (response) in
            self.cartData = response
            self.itemTotal.text = "SAR \(self.cartData.total ?? 0)"
            self.taxRate.text = "SAR \(self.cartData.tax_rate ?? 0)"
            self.deliveryFee.text = "SAR 0"
            self.cartTotal.text = "SAR \(self.cartData.total ?? 0)"
            self.itemTable.reloadData()
        }
    }
    
    func updateItemCount(id: String, qty: Int,type:Int){
        ActivityIndicator.show(view: self.view)
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_UPDATE_CART, method: .post, parameter: ["id":id, "quantity": qty,"delivery_type":type], objectClass: SuccessResponse.self, requestCode: U_UPDATE_CART) { (response) in
            ActivityIndicator.hide()
            Singleton.shared.cartData = GetCartResponse()
            self.callCartAPI()
        }
    }
    
    
    
    //MARK: IBActions
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func addAddressAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "AddAddressViewController") as! AddAddressViewController
        
        self.navigationController?.pushViewController(myVC, animated: true)
    }
    
    
}

extension MyCartViewController: UITableViewDelegate ,  UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(tableView == itemTable){
            return self.cartData.items.count
        }else {
            return self.addressData.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(tableView == itemTable){
            let cell = tableView.dequeueReusableCell(withIdentifier: "ItemTablecell") as! ItemTablecell
            let val = self.cartData.items[indexPath.row]
            cell.segmentControl.layer.cornerRadius = 0
            cell.segmentControl.tintColor = .white
            cell.segmentControl.tintColor = .white
            cell.segmentControl.backgroundColor = .clear
            cell.segmentControl.layer.masksToBounds = true
            // selected option color
            cell.segmentControl.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.black], for: .selected)
            
            // color of other options
            cell.segmentControl.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.white], for: .normal)
            
            cell.itemImage.pin_setImage(from: URL(string: val.business?.logo ?? ""))
            cell.itemName.text = val.business?.name
            cell.itemPrice.text = "SAR \(val.product?.price ?? 0)"
            cell.deleteButton = {
                ActivityIndicator.show(view: self.view)
                SessionManager.shared.methodForApiCalling(url: U_BASE + U_REMOVE_CART_ITEM + "\(val.id ?? "")", method: .post, parameter: nil, objectClass: SuccessResponse.self, requestCode: U_REMOVE_CART_ITEM) { (response) in
                    ActivityIndicator.hide()
                    self.showMsg(controller: self, text: "Product removed successfully")
                    Singleton.shared.cartData = GetCartResponse()
                    self.callCartAPI()
                }
            }
            cell.itemCountLabel.text = "\(val.quantity ?? 0)"
            cell.plusBtn={
                let count = Int(cell.itemCountLabel.text ?? "0")!
                cell.itemCountLabel.text = "\(count + 1)"
            }
            
            cell.minusBtn={
                let count = Int(cell.itemCountLabel.text ?? "0")!
                if(count > 0){
                    cell.itemCountLabel.text = "\(count - 1)"
                }
            }
            
            cell.rapidFireButton.tapHandler={(i) in
                let count = Int(cell.itemCountLabel.text ?? "0")!
                ActivityIndicator.show(view: self.view)
                self.updateItemCount(id: val.id ?? "", qty: count,type:1)
            }
            
            
            cell.rapidFIreButtonTwo.tapHandler={(i) in
                let count = Int(cell.itemCountLabel.text ?? "0")!
                self.updateItemCount(id: val.id ?? "", qty: count,type:1)
            }
            
            return cell
        }else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "AddressTable") as! AddressTable
            let val = self.addressData[indexPath.row]
            cell.streetAddress.text = (val.house ?? "") + " " + (val.street ?? "")
            
            cell.addressImage.backgroundColor = .white
            cell.defaultButton.setTitle("Change", for: .normal)
            self.selectedAddress = val
        
            cell.address.text = (val.location ?? "") + ", " + (val.city ?? "")
            cell.address.text = (cell.address.text ?? "")  + ", " + (val.state ?? "")
            cell.changeBtn = {
                let myVC = self.storyboard?.instantiateViewController(withIdentifier: "AddAddressViewController") as! AddAddressViewController
                self.navigationController?.pushViewController(myVC, animated: true)
            }
            return cell
        }
    }
}

class AddressTable: UITableViewCell {
    //MARK: IBOutlets
    @IBOutlet weak var streetAddress: DesignableUILabel!
    @IBOutlet weak var address: DesignableUILabel!
    @IBOutlet weak var defaultButton: CustomButton!
    @IBOutlet weak var addressImage: UIImageView!
    
    var changeBtn:(()->Void)? = nil
    
    //MARK: IBActions
    @IBAction func changeAction(_ sender: Any) {
        if let changeBtn = self.changeBtn {
            changeBtn()
        }
    }
    
}

class ItemTablecell: UITableViewCell {
    //MARK: IBOutlets
    @IBOutlet weak var itemName: DesignableUILabel!
    @IBOutlet weak var itemPrice: DesignableUILabel!
    @IBOutlet weak var segmentControl: UISegmentedControl!
    @IBOutlet weak var itemImage: ImageView!
    @IBOutlet weak var itemRating: DesignableUILabel!
    @IBOutlet weak var itemDescription: DesignableUILabel!
    @IBOutlet weak var itemStatus: DesignableUILabel!
    @IBOutlet weak var distance: DesignableUILabel!
    @IBOutlet weak var rapidFireButton: RapidFireButton!
    @IBOutlet weak var rapidFIreButtonTwo: RapidFireButton!
    @IBOutlet weak var itemCountLabel: DesignableUILabel!
    @IBOutlet weak var ratingView: CosmosView!
    
    
    
    var addButton:(()-> Void)? = nil
    var deleteButton:(()-> Void)? = nil
    var plusBtn:(()-> Void)? = nil
    var minusBtn:(()-> Void)? = nil
    
    //MARK: IBActions
    @IBAction func addAction(_ sender: Any) {
        if let addButton = self.addButton{
            addButton()
        }
    }
    
    @IBAction func deleteAction(_ sender: Any) {
        if let deleteButton = self.deleteButton{
            deleteButton()
        }
    }
    
    @IBAction func minusAction(_ sender: Any) {
        if let minusBtn = minusBtn{
            minusBtn()
        }
    }
    
    @IBAction func plusAction(_ sender: Any) {
        if let plusBtn = plusBtn{
            plusBtn()
        }
    }
    
}
