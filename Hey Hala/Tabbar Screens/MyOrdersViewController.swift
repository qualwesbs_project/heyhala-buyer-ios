//
//  MyOrdersViewController.swift
//  Hey Hala
//
//  Created by Apple on 31/03/21.
//

import UIKit
import RAMAnimatedTabBarController
import PINRemoteImage

class MyOrdersViewController: UIViewController,UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let  char = string.cString(using: String.Encoding.utf8)!
          let isBackSpace = strcmp(char, "\\b")
        if (isBackSpace == -92 && self.searchField.text!.count == 1) {
            self.filterData = []
            self.orderTable.reloadData()
            
        }else {
        self.filterData = self.orderData.filter{
            (($0.orderItems[0].business?.name ?? "").lowercased().contains(((self.searchField.text ?? "") + string)))
        }
        self.orderTable.reloadData()
        }
        return true
    }
    
    //MARK: IBOutlets
    @IBOutlet weak var myOrderTab: RAMAnimatedTabBarItem!
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var searchView: View!
    @IBOutlet weak var orderTable: ContentSizedTableView!
    @IBOutlet weak var searchField: UITextField!
    
    var showBackButton = false
    var orderData = [OrderResponse]()
    var filterData = [OrderResponse]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if(showBackButton){
            self.searchView.isHidden = true
            self.backView.isHidden = false
        }
        self.myOrderTab.animation.iconSelectedColor = .white
        self.myOrderTab.animation.textSelectedColor = .white
        myOrderTab.playAnimation()

    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.getOrderData()
    }
    
    func getOrderData(){
        ActivityIndicator.show(view: self.view)
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_GET_ORDERS, method: .get, parameter: nil, objectClass: GetOrders.self, requestCode: U_GET_ORDERS) { (response) in
            self.orderData = response.response
            ActivityIndicator.hide()
            self.orderTable.reloadData()
        }
    }
    
    //MARK: IBActions
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}

extension MyOrdersViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.orderData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "OrderTableCell") as! OrderTableCell
        let val = self.orderData[indexPath.row]
        
        cell.orderId.text = "\(val.order_id ?? 0)"
        cell.orderDate.text = String((val.created_at ?? "").prefix(10))
        cell.itemImage.pin_setImage(from: URL(string: val.orderItems[0].business?.logo ?? ""))
        cell.itemRating.text = "\(val.orderItems[0].business?.review?.rating ?? 0)"
        cell.itemName.text = val.orderItems[0].business?.name
        cell.itemPrice.text = "SAR \(val.total ?? 0)"
        cell.itemDescription.text = ""
        for item in val.orderItems{
            if(cell.itemDescription.text!.isEmpty){
                cell.itemDescription.text =  (cell.itemDescription.text ?? "") + ((item.product?.name ?? "") + " x\(item.quantity ?? 1)")
            }else {
                let text = (cell.itemDescription.text ?? "")  + " ,"
                cell.itemDescription.text = text +  ((item.product?.name ?? "") + " x\(item.quantity ?? 1)")
            }
            
        }
        cell.deliveryDate.text = String((val.created_at ?? "").prefix(10))
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "OrderDetailViewController") as! OrderDetailViewController
        myVC.orderData = self.orderData[indexPath.row]
        self.navigationController?.pushViewController(myVC, animated: true)
    }
}
