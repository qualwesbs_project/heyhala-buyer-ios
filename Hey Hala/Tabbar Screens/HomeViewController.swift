//
//  HomeViewController.swift
//  Hey Hala
//
//  Created by Apple on 31/03/21.
//

import UIKit
import RAMAnimatedTabBarController
import ViewAnimator
import PINRemoteImage

class HomeViewController: UIViewController, UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let  char = string.cString(using: String.Encoding.utf8)!
          let isBackSpace = strcmp(char, "\\b")
        if (isBackSpace == -92 && self.searchField.text!.count == 1) {
            self.filterData = []
            self.collectionView.reloadData()
            
        }else {
        self.filterData = self.categoryData.filter{
            (($0.category_name ?? "").lowercased().contains(((self.searchField.text ?? "") + string)))
        }
        self.collectionView.reloadData()
        }
        return true
    }
    
    //MARK: IBOutlets
    @IBOutlet weak var homeTab: RAMAnimatedTabBarItem!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var searchVIew: UIView!
    @IBOutlet weak var searchField: UITextField!
    
    
    var categoryData = [GetCategoryResponse]()
    var filterData = [GetCategoryResponse]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.searchField.delegate = self
        homeTab.animation.textSelectedColor = .white
        homeTab.animation.iconSelectedColor = .white
        homeTab.playAnimation()
        NavigationController.shared.getAllCategory(view: self) { (response) in
            self.categoryData = response
            self.collectionView.reloadData()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.animate()
    }
    
    @IBAction func animate() {
        // Combined animations example
        let fromAnimation = AnimationType.vector(CGVector(dx: 30, dy: 0))
        let zoomAnimation = AnimationType.zoom(scale: 0.2)
        let rotateAnimation = AnimationType.rotate(angle: CGFloat.pi/6)
        
        UIView.animate(views: collectionView.visibleCells,
                       animations: [fromAnimation], delay: 0)
        
    }
    
    
    //MARK: IBActions
    
    @IBAction func signupAction(_ sender: Any) {
    }
    
    @IBAction func languageAction(_ sender: Any) {
    }
    
    @IBAction func clearAction(_ sender: UIButton) {
        self.searchVIew.isHidden = sender.tag == 1 ? false:true
        if(sender.tag == 2){
            self.filterData = []
            self.searchField.text = ""
            self.collectionView.reloadData()
        }else {
            self.collectionView.becomeFirstResponder()
        }
    }
    
}

extension HomeViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if(filterData.count > 0){
            return self.filterData.count
        }else {
            return self.categoryData.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CategoryCollection", for: indexPath) as!  CategoryCollection
        let val = self.filterData.count > 0 ? self.filterData[indexPath.row]:self.categoryData[indexPath.row]
        cell.categoryName.text = val.category_name
        cell.categoryDesc.text = "\(val.count ?? 0)"
        cell.categoryImage.pin_setImage(from: URL(string:U_IMAGE_BASE + (val.image ?? "")))
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "CategoryViewController") as! CategoryViewController
        myVC.selectedCategory = self.categoryData[indexPath.row]
        if(self.categoryData.count > 0){
            self.navigationController?.pushViewController(myVC, animated: true)
        }else {
            self.showMsg(controller: self, text: "No business found")
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (collectionView.frame.width/2)-15, height: (collectionView.frame.width/2)-15)
    }
    
    
}


class CategoryCollection: UICollectionViewCell{
    //MARK: IBOutlets
    @IBOutlet weak var categoryImage: ImageView!
    @IBOutlet weak var categoryName: DesignableUILabel!
    @IBOutlet weak var categoryDesc: DesignableUILabel!
}
