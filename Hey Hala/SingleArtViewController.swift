//
//  SingleArtViewController.swift
//  Hey Hala
//
//  Created by Sagar Pandit on 13/05/21.
//

import UIKit
import Cosmos
import PINRemoteImage

class SingleArtViewController: UIViewController, UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        if(textView.text == "Description here..."){
            self.textView.text = ""
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if(textView.text == ""){
            self.textView.text = "Description here..."
        }
    }
    //MARK: IBOutlets
    @IBOutlet weak var orderButton: DesignableUILabel!
    @IBOutlet weak var itemDescription: DesignableUILabel!
    @IBOutlet weak var itemPrice: DesignableUILabel!
    @IBOutlet weak var itemDeliveryDate: DesignableUILabel!
    @IBOutlet weak var itemName: DesignableUILabel!
    @IBOutlet weak var itemDetailStack: UIStackView!
    @IBOutlet weak var reviewStackView: UIStackView!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var ratingView: CosmosView!
    @IBOutlet weak var itemImage: ImageView!
    
    
    var selectedArt = GetStoreResponse()
    var isReviewProduct = false

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initializeView()
    }
    
    func initializeView() {
        self.textView.delegate = self
        self.itemDetailStack.isHidden = isReviewProduct
        self.reviewStackView.isHidden = !isReviewProduct
        self.orderButton.text = isReviewProduct ? "Submit review":"Order now"
        self.itemImage.pin_setImage(from: URL(string: U_IMAGE_BASE + (self.selectedArt.logo ?? self.selectedArt.image ?? "")))
        self.itemName.text = self.selectedArt.name
        self.itemDescription.text = self.selectedArt.description
        self.itemPrice.text = "SAR \(self.selectedArt.price ?? 0).00"
        self.itemDeliveryDate.text = "\(self.selectedArt.preprationTime ?? 0) minutes"
    }
    
    //MARK: IBActions
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func orderAction(_ sender: Any) {
        if(self.isReviewProduct){
            if(self.ratingView.rating == 0){
                self.showMsg(controller: self, text: "Please give rating")
            }else if(self.textView.text!.isEmpty || (textView.text == "Description here...")){
                self.showMsg(controller: self, text: "Enter description")
            }else {
                ActivityIndicator.show(view: self.view)
                var param = [String:Any]()
                if(self.selectedArt.business == nil){
                    param = [
                        "business":self.selectedArt.id ?? "",
                        "score": self.ratingView.rating,
                        "description": self.textView.text
                    ]
                }else {
                    param = [
                        "product": self.selectedArt.id ?? "",
                        "business":self.selectedArt.business ?? "",
                        "score": self.ratingView.rating,
                        "description": self.textView.text
                    ]
                }
                
                SessionManager.shared.methodForApiCalling(url: U_BASE + U_ADD_REVIEW, method: .post, parameter: param, objectClass: SuccessResponse.self, requestCode: U_ADD_REVIEW) { (response) in
                    self.showMsg(controller: self, text: "Review addedd successfully")
                    self.ratingView.rating = 0
                    self.textView.text = "Description here..."
                    ActivityIndicator.hide()
                    let myVC = self.storyboard?.instantiateViewController(withIdentifier: "TabbarViewController") as! TabbarViewController
                    self.navigationController?.pushViewController(myVC, animated: true)
                }
            }
        }else {
            let myVC = self.storyboard?.instantiateViewController(withIdentifier: "OrderConfirmationViewController") as! OrderConfirmationViewController
            self.navigationController?.pushViewController(myVC, animated: true)
        }
        
    }
}
