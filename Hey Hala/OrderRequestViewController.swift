//
//  OrderRequestViewController.swift
//  Hey Hala
//
//  Created by Sagar Pandit on 13/05/21.
//

import UIKit
import GooglePlaces
import GoogleMaps
import MapKit

class OrderRequestViewController: UIViewController , SelectDate {
    func selectedDate(timestamp: Date) {
        if(self.currentDatePicker == 1){
            self.dateField.text = self.convertTimestampToDate(Int(timestamp.timeIntervalSince1970), to: "yyyy-MM-dd")
            self.selectedDate = timestamp
        }else if(self.currentDatePicker == 2){
            self.startField.text = self.convertTimestampToDate(Int(timestamp.timeIntervalSince1970), to: "h:mm a")
            self.selectedStartTime = timestamp
        }else if(self.currentDatePicker == 3){
            self.endField.text = self.convertTimestampToDate(Int(timestamp.timeIntervalSince1970), to: "h:mm a")
            self.selectedEndTime = timestamp
        }
        
    }
    //MARK: IBOutlets
    @IBOutlet weak var dateField: UITextField!
    @IBOutlet weak var startField: UITextField!
    @IBOutlet weak var endField: UITextField!
    @IBOutlet weak var locationField: UITextField!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var requestStack: UIStackView!
    @IBOutlet weak var submitButton: CustomButton!
    @IBOutlet weak var rejectButton: CustomButton!
    @IBOutlet weak var amountField: UITextField!
    @IBOutlet weak var amountView: View!
    
    
    var currentDatePicker = Int()
    var latitude = CLLocationDegrees()
    var longitude = CLLocationDegrees()
    var selectedDate = Date()
    var selectedStartTime = Date()
    var selectedEndTime = Date()
    var selectedProduct = GetStoreResponse()
    var isEventRequest = false
    var serviceRequestData = ServiceRequestResponse()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        dateField.attributedPlaceholder = NSAttributedString(string: "Date",
                                                             attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        
        startField.attributedPlaceholder = NSAttributedString(string: "Start Time",
                                                              attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        locationField.attributedPlaceholder = NSAttributedString(string: "Location",
                                                                 attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        endField.attributedPlaceholder = NSAttributedString(string: "End Time",
                                                            attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        amountField.attributedPlaceholder = NSAttributedString(string: "Amount",
                                                            attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        if(isEventRequest){
            self.requestStack.isUserInteractionEnabled = false
            self.dateField.text = self.convertTimestampToDate(self.serviceRequestData.date ?? 0, to: "yyyy-MM-dd")
            self.startField.text = self.convertTimestampToDate(self.serviceRequestData.startTime ?? 0, to: "h:mm a")
            self.endField.text = self.convertTimestampToDate(self.serviceRequestData.endTime ?? 0, to: "h:mm a")
            self.amountField.text = "SAR \(self.serviceRequestData.amount ?? 0)"
            self.locationField.text =  self.serviceRequestData.location
            self.textView.text = self.serviceRequestData.description
            if(self.serviceRequestData.status?.status == "2"){
                self.submitButton.setTitle("Accept", for: .normal)
                self.rejectButton.isHidden = false
                self.amountView.isHidden = false
            }else if(self.serviceRequestData.status?.status == "1"){
                self.submitButton.setTitle("Pending ...", for: .normal)
                self.submitButton.isUserInteractionEnabled = false
                self.rejectButton.isHidden = true
                self.amountView.isHidden = true
            }else {
                self.submitButton.isHidden = true
                self.rejectButton.isHidden = true
                self.amountView.isHidden = true
            }
        }
    }
    
    //MARK: IBActions
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func dateAction(_ sender: UIButton) {
        self.currentDatePicker = sender.tag
        if(self.dateField.text!.isEmpty && sender.tag != 1){
            self.showMsg(controller: self, text: "Select date")
            return
        }
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "DatePickerViewController") as! DatePickerViewController
        myVC.minDate = self.selectedDate
        myVC.pickerMode = self.currentDatePicker == 1 ? .date:.time
        myVC.modalPresentationStyle = .overFullScreen
        myVC.dateDelegate = self
        self.navigationController?.present(myVC, animated: true, completion: nil)
    }
    
    @IBAction func locationAction(_ sender: Any) {
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        
        // Specify the place data types to return.
        let fields: GMSPlaceField = GMSPlaceField(rawValue:UInt(GMSPlaceField.name.rawValue) |
                                                    UInt(GMSPlaceField.placeID.rawValue) |
                                                    UInt(GMSPlaceField.coordinate.rawValue) |
                                                    GMSPlaceField.addressComponents.rawValue |
                                                    GMSPlaceField.formattedAddress.rawValue)
        autocompleteController.placeFields = fields
        
        // Specify a filter.
        let filter = GMSAutocompleteFilter()
        filter.type = .address
        autocompleteController.autocompleteFilter = filter
        
        // Display the autocomplete view controller.
        autocompleteController.modalPresentationStyle = .overFullScreen
        present(autocompleteController, animated: true, completion: nil)
    }
    
    @IBAction func submitRequestAction(_ sender: Any) {
        if(self.isEventRequest){
            
        }else {
        if(self.dateField.text!.isEmpty){
            self.showMsg(controller: self, text: "Select date")
        }else if(self.startField.text!.isEmpty){
            self.showMsg(controller: self, text: "Select start time")
        }else if(self.endField.text!.isEmpty){
            self.showMsg(controller: self, text: "Select end time")
        }else if(self.locationField.text!.isEmpty){
            self.showMsg(controller: self, text: "Select location")
        }else {
            ActivityIndicator.show(view: self.view)
            let param:[String:Any] = [
                "service": self.selectedProduct.id ?? "",
                "date": Int(self.selectedDate.timeIntervalSince1970),
                "startTime": Int(self.selectedStartTime.timeIntervalSince1970),
                "endTime": Int(self.selectedEndTime.timeIntervalSince1970),
                "location": self.locationField.text ?? "",
                "latitude": self.latitude,
                "longitude":self.longitude,
                "description": self.textView.text
            ]
            
            SessionManager.shared.methodForApiCalling(url: U_BASE + U_REQUEST_SERVICE, method: .post, parameter: param, objectClass: SuccessResponse.self, requestCode: U_REQUEST_SERVICE) { (response) in
                ActivityIndicator.hide()
                self.showMsg(controller: self, text: "Request submitted successfully")
                let myVC = self.storyboard?.instantiateViewController(withIdentifier: "TabbarViewController") as! TabbarViewController
                self.navigationController?.pushViewController(myVC, animated: true)
            }
        }
        }
    }
    
    @IBAction func rejectAction(_ sender: Any) {
    }
    
    
}


extension OrderRequestViewController: GMSAutocompleteViewControllerDelegate    {
    
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        dismiss(animated: true, completion: nil)
        self.locationField.text = place.formattedAddress
        self.latitude = place.coordinate.latitude
        self.longitude = place.coordinate.longitude
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
}
