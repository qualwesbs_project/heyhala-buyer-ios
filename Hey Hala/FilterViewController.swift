//
//  FilterViewController.swift
//  Hey Hala
//
//  Created by Apple on 16/04/21.
//

import UIKit

@objc protocol FilterBusiness {
    @objc optional  func refreshBusinessTable(type:Int)
}

class FilterViewController: UIViewController {
   //MARK: IBOUtlets
    @IBOutlet weak var imageOne: ImageView!
    @IBOutlet weak var imageTwo: ImageView!
    @IBOutlet weak var imageThree: ImageView!
    @IBOutlet weak var imageFour: ImageView!
    @IBOutlet weak var imageFive: ImageView!
    
    var sortType = Int()
    var filterDelegate: FilterBusiness? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if(self.sortType == 1){
           // self.imageOne.backgroundColor = primaryColor
            self.imageFive.backgroundColor = primaryColor
        }else if(self.sortType == 2){
          //  self.imageTwo.backgroundColor = primaryColor
            self.imageFour.backgroundColor = primaryColor
        }else if(self.sortType == 3){
            self.imageThree.backgroundColor = primaryColor
        }else if(self.sortType == 4){
            self.imageFour.backgroundColor = primaryColor
        }else if(self.sortType == 5){
            self.imageFive.backgroundColor = primaryColor
        }
    }
    
    func initializeView(image: ImageView){
        self.imageOne.backgroundColor = .white
        self.imageTwo.backgroundColor = .white
        self.imageThree.backgroundColor = .white
        self.imageFour.backgroundColor = .white
        self.imageFive.backgroundColor = .white
        image.backgroundColor = primaryColor
    }
    
   
    //MARK: IBAction
    @IBAction func closeAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func selectOption(_ sender: UIButton) {
        if(sender.tag == 1){
            sortType = sender.tag
           // self.initializeView(image: imageOne)
            self.initializeView(image: imageFive)
        }else if(sender.tag == 2){
            sortType = sender.tag
           // self.initializeView(image: imageTwo)
            self.initializeView(image: imageFour)
        }else if(sender.tag == 3){
            sortType = sender.tag
            self.initializeView(image: imageThree)
        }else if(sender.tag == 4){
            sortType = sender.tag
            self.initializeView(image: imageFour)
        }else if(sender.tag == 5){
            sortType = sender.tag
            self.initializeView(image: imageFive)
        }else if(sender.tag == 6){
            self.filterDelegate?.refreshBusinessTable?(type: self.sortType)
            self.dismiss(animated: true, completion: nil)
        }
    }
    
}
