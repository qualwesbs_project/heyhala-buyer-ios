//
//  NavigationController.swift
//  Hey Hala
//
//  Created by qw on 01/01/21.
//

import UIKit
import CoreLocation


class NavigationController: UIViewController, CLLocationManagerDelegate {
    //MARK: IBOutlets
    
    
    static let shared = NavigationController()
    lazy var locationManager = CLLocationManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
    }
    
    func getAllCategory(view: UIViewController,completionHandler: @escaping ([GetCategoryResponse]) -> Void){
        if(Singleton.shared.allCategoryData.count == 0){
            ActivityIndicator.show(view: view.view)
            SessionManager.shared.methodForApiCalling(url: U_BASE + U_GET_CATEGORIES, method: .get, parameter: nil, objectClass: GetCategories.self, requestCode: U_GET_CATEGORIES) { (response) in
                Singleton.shared.allCategoryData = response.response
                completionHandler(Singleton.shared.allCategoryData)
                ActivityIndicator.hide()
            }
        }else {
            completionHandler(Singleton.shared.allCategoryData)
        }
    }
    
    func getAllAddress(view: UIViewController,completionHandler: @escaping ([AddressResponse]) -> Void){
        if(Singleton.shared.allAddressData.count == 0){
            ActivityIndicator.show(view: view.view)
            SessionManager.shared.methodForApiCalling(url: U_BASE + U_GET_ADDRESS, method: .get, parameter: nil, objectClass: GetAddress.self, requestCode: U_GET_ADDRESS) { (response) in
                Singleton.shared.allAddressData = response.response
                completionHandler(Singleton.shared.allAddressData)
                ActivityIndicator.hide()
            }
        }else {
            completionHandler(Singleton.shared.allAddressData)
        }
    }
    
    func getcartData(view: UIViewController,completionHandler: @escaping (GetCartResponse) -> Void){
        if(Singleton.shared.cartData.items.count == 0){
            ActivityIndicator.show(view: view.view)
            SessionManager.shared.methodForApiCalling(url: U_BASE + U_GET_CART_ITEMS, method: .get, parameter: nil, objectClass: GetCartItem.self, requestCode: U_GET_CART_ITEMS) { (response) in
                Singleton.shared.cartData = response.response
                completionHandler(Singleton.shared.cartData)
                ActivityIndicator.hide()
            }
        }else {
            completionHandler(Singleton.shared.cartData)
        }
    }    
}
