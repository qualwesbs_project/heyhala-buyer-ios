//
//  Constants.swift
//  Hey Hala
//
//  Created by qw on 23/12/20.

import UIKit

let primaryColor = UIColor(red: 111/255, green: 1/255, blue: 1/255, alpha: 1)//#6F0101
let darkBrown = UIColor(red: 90/255, green: 2/255, blue: 2/255, alpha: 1) //#5A0202
let blackColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 1) //#000000
let darkGrayColor = UIColor(red: 44/255, green: 44/255, blue: 44/255, alpha: 1) //#2C2C2C
let grayColor = UIColor(red: 58/255, green: 56/255, blue: 56/255, alpha: 1) //#3A3838

let U_BASE = "http://18.116.162.191:3333/"
let U_IMAGE_BASE =  "http://18.116.162.191:3333"
let U_UPLOAD_IMAGE = "file"

let U_LOGIN = "auth/login"
let U_SIGN_UP = "buyer/auth/signup"
let U_GET_OTP = "otp/"
let U_VERIFY_OTP = "otp"

let U_MARK_BUSINESS_FAVOURITE = "favourite-business"
let U_CHECK_BUSINESS_FAVOURITE = "favourite-business/"
let U_GET_FAVOURITE_BUSINESS = "favourite-business"
let U_UNMARK_BUSINESS_FAVOURITE = "favourite-business-remove"

let U_GET_CATEGORIES = "categories"
let U_GET_CATEGORY_BUSINESS = "category-business/"
let U_GET_BUSINESS_PRODUCT = "business-products/"
let U_MARK_PRODUCT_FAVOURITE = "favourite-product"
let U_GET_FAVOURITE_PRODUCT = "favourite-product"
let U_CHECK_FAVOURITE_PRODUCT = "favourite-product/"
let U_REMOVE_FAVOURITE_PRODUCT = "favourite-product-remove"
let U_GET_SAVED_WISHLIST = "favourite-product"
let U_DELETE_WISHLIST_PRODUCT = "favourite-product-remove"

let U_GET_REQUEST_SERVICE = "service-requested-user"
let U_ADD_REQUEST_SERVICE = "service-request"

let U_ADD_TO_CART = "add-to-cart"
let U_GET_CART_ITEMS = "get-cart"
let U_UPDATE_CART = "update-cart"
let U_REMOVE_CART_ITEM = "remove-cart/"

let U_PLACE_ORDER = "order"
let U_GET_ORDERS = "orders"
let U_REQUEST_SERVICE = "service-request"


let U_ADD_ADDRESS = "address"
let U_GET_ADDRESS = "address"
let U_SET_DEFAULT_ADDRESS = "set-default-address/"
let U_DELETE_ADDRESS = "address-delete/"

let U_ADD_REVIEW = "review"
let U_GET_PRODUCT_REVIEW = "review-product/"
let U_GET_BUSINESS_REVIEW = "review-business/"

//User Defaults
let UD_USER_DETAIl = "userDetail"
let UD_TOKEN = "user_token"

//Notifications
let N_SHOW_ERROR_MESSAGE = "N_SHOW_ERROR_MESSAGE"

