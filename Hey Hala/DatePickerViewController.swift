//
//  DatePickerViewController.swift
//  Hey Hala
//
//  Created by Sagar Pandit on 16/06/21.
//

import UIKit

protocol SelectDate {
    func selectedDate(timestamp:Date)
}

class DatePickerViewController: UIViewController {
    //MARK: IBOutlets
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var headingLabel: DesignableUILabel!
    @IBOutlet weak var pickupButton: CustomButton!
    @IBOutlet weak var deliveryButton: CustomButton!
    
    static var shared = DatePickerViewController()
    var dateDelegate: SelectDate? = nil
    var pickerMode: UIDatePicker.Mode?
    var minDate: Date?
    var maxDate: Date?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        datePicker.datePickerMode = (pickerMode != nil) ? pickerMode as! UIDatePicker.Mode:.date
        if(minDate != nil){
            datePicker.minimumDate = minDate
        }
        if(maxDate != nil){
            datePicker.maximumDate = maxDate
        }
    }
    
    //MARK: IBActions
    @IBAction func doneButton(_ sender: CustomButton) {
       // isSelfPickup = sender.tag == 1 ? true:false
        self.dateDelegate?.selectedDate(timestamp: datePicker.date)
        self.dismiss(animated: true, completion: nil)
    }
}
