//
//  CategoryDetailViewController.swift
//  Hey Hala
//
//  Created by Apple on 07/04/21.
//

import UIKit

class CategoryDetailViewController: UIViewController {
    //MARK: IBOutlets
    @IBOutlet weak var bgImage: UIImageView!
    @IBOutlet weak var storeName: DesignableUILabel!
    @IBOutlet weak var storeDistance: DesignableUILabel!
    @IBOutlet weak var storeDescription: DesignableUILabel!
    @IBOutlet weak var productTable: ContentSizedTableView!
    @IBOutlet weak var heartImage: UIImageView!
    @IBOutlet weak var reviewLabel: DesignableUILabel!
    @IBOutlet weak var reviewRating: DesignableUILabel!
    
    
    var productData = [GetStoreResponse]()
    var selectedStore = GetStoreResponse()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initializeView()
    }
    
    func initializeView(){
        self.checkStoreFavourite()
        self.getStoreProducts()
        self.bgImage.pin_setImage(from: URL(string: U_IMAGE_BASE + (self.selectedStore.logo ?? "")))
        self.storeName.text = self.selectedStore.name
        self.storeDescription.text = self.selectedStore.description
        self.reviewLabel.text = "\(self.selectedStore.review?.reviews ?? 0) reviews"
        self.reviewRating.text = "\(self.selectedStore.review?.rating ?? 0)"
        self.storeDistance.text = "\(self.selectedStore.distance ?? "") kms"
    }
    
    func checkStoreFavourite(){
        ActivityIndicator.show(view: self.view)
        
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_CHECK_BUSINESS_FAVOURITE + "\(self.selectedStore.id ?? "")" , method: .get, parameter: nil, objectClass: CheckFavourite.self, requestCode: U_CHECK_BUSINESS_FAVOURITE) { (response) in
            ActivityIndicator.hide()
            if(response.response?.is_favourite == 1){
                self.heartImage.image = #imageLiteral(resourceName: "like")
            }else {
                self.heartImage.image = #imageLiteral(resourceName: "heart_outline")
            }
        }
    }
    
    func getStoreProducts(){
        ActivityIndicator.show(view: self.view)
        
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_GET_BUSINESS_PRODUCT + "\(self.selectedStore.id ?? "")" , method: .get, parameter: nil, objectClass: GetStores.self, requestCode: U_GET_CATEGORY_BUSINESS) { (response) in
            self.productData = response.response
            self.productTable.reloadData()
            ActivityIndicator.hide()
            
        }
    }
    
    //MARK: IBActions
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func heartAction(_ sender: Any) {
        var url = String()
        if(heartImage.image == #imageLiteral(resourceName: "heart_outline")){
            self.heartImage.image = #imageLiteral(resourceName: "like")
            url = U_BASE + U_MARK_BUSINESS_FAVOURITE
        }else {
            self.heartImage.image = #imageLiteral(resourceName: "heart_outline")
            url = U_BASE + U_UNMARK_BUSINESS_FAVOURITE
        }
        
        SessionManager.shared.methodForApiCalling(url: url, method:.post, parameter:["business":self.selectedStore.id], objectClass: CheckFavourite.self, requestCode: url) { (response) in
            if(self.heartImage.image != #imageLiteral(resourceName: "heart_outline")){
                self.showMsg(controller: self, text: "Successfully marked as favourite")
            }else {
                self.showMsg(controller: self, text: "Removed from favourite")
            }
            ActivityIndicator.hide()
        }
    }
    
    @IBAction func reviewAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "RateReviewViewController") as! RateReviewViewController
        myVC.produtDetail = self.selectedStore
        self.navigationController?.pushViewController(myVC, animated: true)
    }
    
    @IBAction func reviewSellerAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "SingleArtViewController") as! SingleArtViewController
        myVC.selectedArt = self.selectedStore
        myVC.isReviewProduct = true
        self.navigationController?.pushViewController(myVC, animated: true)
    }
}

extension CategoryDetailViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.productData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let val = self.productData[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "ItemTablecell") as! ItemTablecell
        cell.itemImage.pin_setImage(from: URL(string: U_IMAGE_BASE + (val.image ?? "") ))
        cell.itemDescription.text = val.description
        cell.itemName.text = val.name
        cell.itemStatus.text = "\(val.preprationTime ?? 0) min"
        cell.itemRating.text =  "\(val.review?.rating ?? 0)"
        cell.distance.text = "\(val.distance ?? "0") kms"
        cell.itemPrice.text = "SAR \(val.price ?? 0).00"
        cell.addButton={
            ActivityIndicator.show(view: self.view)
            let param:[String:Any] = [
                "product": val.id ?? "",
                "quantity": 1,
                "business":val.business ?? "",
            ]
            SessionManager.shared.methodForApiCalling(url: U_BASE + U_ADD_TO_CART, method: .post, parameter: param, objectClass: SuccessResponse.self, requestCode: U_ADD_TO_CART) { (response) in
                ActivityIndicator.hide()
                Singleton.shared.cartData = GetCartResponse()
                self.showMsg(controller: self, text: "Successfully added to cart")
                self.getStoreProducts()
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "OrderNowViewController") as! OrderNowViewController
        myVC.selectedProduct = self.productData[indexPath.row]
        self.navigationController?.pushViewController(myVC, animated: true)
    }
    
}
