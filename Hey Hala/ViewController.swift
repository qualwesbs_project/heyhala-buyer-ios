//
//  ViewController.swift
//  Hey Hala
//
//  Created by Apple on 25/03/21.
//

import UIKit

class ViewController: UIViewController {
    //MARK: IBOutlets
    

    override func viewDidLoad() {
        super.viewDidLoad()
        if let token = UserDefaults.standard.value(forKey: UD_TOKEN) as? String{
            let myVC  = self.storyboard?.instantiateViewController(withIdentifier: "TabbarViewController") as! TabbarViewController
            self.navigationController?.pushViewController(myVC, animated: true)
        }else {
        let myVC  = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        self.navigationController?.pushViewController(myVC, animated: true)
        }
        NotificationCenter.default.addObserver(self, selector: #selector(self.showErrorMessage(_:)), name: NSNotification.Name(N_SHOW_ERROR_MESSAGE), object: nil)
    }
    
    @objc func showErrorMessage(_ notif: Notification){
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(N_SHOW_ERROR_MESSAGE), object: nil)
        guard let val = notif.userInfo?["msg"] as? String else {
            return
        }
        self.showMsg(controller: self, text: val)
        if ((notif.userInfo?["requestCode"] as? String) == U_VERIFY_OTP && val != "Wrong Otp") {
            self.navigationController?.popViewController(animated: true)
        }
        NotificationCenter.default.addObserver(self, selector: #selector(self.showErrorMessage(_:)), name: NSNotification.Name(N_SHOW_ERROR_MESSAGE), object: nil)
    }
}
    
