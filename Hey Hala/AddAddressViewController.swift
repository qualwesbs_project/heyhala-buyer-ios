//
//  AddAddressViewController.swift
//  Hey Hala
//
//  Created by Apple on 17/04/21.
//

import UIKit

class AddAddressViewController: UIViewController, AddNewAddress {
    func refreshAddressTable() {
        Singleton.shared.allAddressData = []
        NavigationController.shared.getAllAddress(view: self) { (response) in
            self.addressData = response
            self.addressTable.reloadData()
        }
    }
    
    //MARK: IBOutlets
    @IBOutlet weak var addressTable: UITableView!
    

    var addressData = [AddressResponse]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NavigationController.shared.getAllAddress(view: self) { (response) in
            self.addressData = response
            self.addressTable.reloadData()
        }
    }
    
    //MARK: IBActions
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func addAddressAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "MapViewController") as! MapViewController
        myVC.addressDelegate = self
        self.navigationController?.pushViewController(myVC, animated: true)
    }
    
    
}

extension AddAddressViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.addressData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AddressTableCell") as! AddressTableCell
        let val = self.addressData[indexPath.row]
        cell.streetLabel.text = "\(val.postal_code ?? "") \(val.house ?? "") \(val.location ?? "")"
        if(val.is_default_address == 1){
            cell.tickImage.backgroundColor = primaryColor
            cell.primaryAddressLabel.text = "Primary address"
        }else {
            cell.tickImage.backgroundColor = .white
            cell.primaryAddressLabel.text = "Set as primary address"
        }
        
        cell.tickButton={
            let alert = UIAlertController(title:  "Are you sure?", message:"Set as default Address", preferredStyle: .alert)
            
            let action1 = UIAlertAction(title: "Ok", style: .default) { (action) in
                ActivityIndicator.show(view: self.view)
                SessionManager.shared.methodForApiCalling(url: U_BASE + U_SET_DEFAULT_ADDRESS + "\(val.id ?? "")", method: .get, parameter: nil, objectClass: SuccessResponse.self, requestCode: U_SET_DEFAULT_ADDRESS) { (response) in
                    ActivityIndicator.hide()
                    self.showMsg(controller: self, text: "Successfully marked as primary address")
                    self.refreshAddressTable()
                }
            }
            
            let action2 = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
                
            }
            
            alert.addAction(action1)
            alert.addAction(action2)
            self.present(alert, animated: true, completion: nil)
        }
        
        cell.deleteButton={
            let alert = UIAlertController(title:  "Are you sure?", message:"Delete selected Address", preferredStyle: .alert)
            
            let action1 = UIAlertAction(title: "Ok", style: .default) { (action) in
                ActivityIndicator.show(view: self.view)
                SessionManager.shared.methodForApiCalling(url: U_BASE + U_DELETE_ADDRESS + "\(val.id ?? "")", method: .post, parameter: nil, objectClass: SuccessResponse.self, requestCode: U_DELETE_ADDRESS) { (response) in
                    ActivityIndicator.hide()
                    self.showMsg(controller: self, text: "Successfully deleted address")
                    self.refreshAddressTable()
                }
            }
            
            let action2 = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
                
            }
            
            alert.addAction(action1)
            alert.addAction(action2)
            self.present(alert, animated: true, completion: nil)
        }
        
        return cell
    }
    
    
}

class AddressTableCell: UITableViewCell {
    //MARK: IBOutlets
    @IBOutlet weak var tickImage: ImageView!
    @IBOutlet weak var streetLabel: DesignableUILabel!
    @IBOutlet weak var addressLabel: DesignableUILabel!
    @IBOutlet weak var primaryAddressLabel: DesignableUILabel!
    
    var deleteButton:(()-> Void)? = nil
    var tickButton:(()-> Void)? = nil
    
    //MARK: IBActions
    @IBAction func deleteAction(_ sender: Any) {
        if let deleteButton = deleteButton {
            deleteButton()
        }
    }
    
    @IBAction func tickAction(_ sender: Any) {
        if let tickButton = tickButton {
            tickButton()
        }
    }
    
    
    
}
