//
//  MapViewController.swift
//  Hey Hala
//
//  Created by Apple on 17/04/21.
//

import UIKit
import GooglePlaces
import GoogleMaps
import MapKit

protocol AddNewAddress {
    func refreshAddressTable()
}

class MapViewController: UIViewController {
    //MARK: IBOutlets
    @IBOutlet weak var addressField: UITextField!
    @IBOutlet weak var mapView: MKMapView!
    
    
    var latitude = CLLocationDegrees()
    var longitude = CLLocationDegrees()
    var address:CLPlacemark?
    var addressDelegate: AddNewAddress? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.mapView.delegate = self
    }
    
    //MARK: IBActions
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func locationAction(_ sender: Any) {
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        
        // Specify the place data types to return.
        let fields: GMSPlaceField = GMSPlaceField(rawValue:UInt(GMSPlaceField.name.rawValue) |
                                                    UInt(GMSPlaceField.placeID.rawValue) |
                                                    UInt(GMSPlaceField.coordinate.rawValue) |
                                                    GMSPlaceField.addressComponents.rawValue |
                                                    GMSPlaceField.formattedAddress.rawValue)
        autocompleteController.placeFields = fields
        
        // Specify a filter.
        let filter = GMSAutocompleteFilter()
        filter.type = .address
        autocompleteController.autocompleteFilter = filter
        
        // Display the autocomplete view controller.
        autocompleteController.modalPresentationStyle = .overFullScreen
        present(autocompleteController, animated: true, completion: nil)
    }
    
    @IBAction func saveAction(_ sender: Any) {
        if(addressField.text!.isEmpty){
            self.showMsg(controller: self, text: "Enter address")
        }else {
            
            ActivityIndicator.show(view: self.view)
            let param:[String:Any] =
            [
                "longitude": self.latitude,
                "latitude": self.longitude,
                "location": self.address?.thoroughfare,
                "house": self.address?.subLocality,
                "street": self.address?.locality,
                "city":self.address?.addressDictionary?["City"],
                "state": self.address?.addressDictionary?["State"],
                "postalCode": self.address?.postalCode
            ]
            SessionManager.shared.methodForApiCalling(url: U_BASE + U_ADD_ADDRESS, method: .post, parameter: param, objectClass: SuccessResponse.self, requestCode: U_ADD_ADDRESS) { (response) in
                ActivityIndicator.hide()
                self.showMsg(controller: self, text: "Address added successfully")
                self.addressDelegate?.refreshAddressTable()
                self.navigationController?.popViewController(animated: true)
            }
        }
    }

    @IBAction func userLocationAction(_ sender: Any) {
        self.setMapCenter(lat: Singleton.shared.userLocation.latitude, long: Singleton.shared.userLocation.longitude)
        self.latitude = Singleton.shared.userLocation.latitude
        self.longitude = Singleton.shared.userLocation.longitude
    }
    
}

extension MapViewController: GMSAutocompleteViewControllerDelegate, MKMapViewDelegate    {
    
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        dismiss(animated: true, completion: nil)
        self.addressField.text = place.formattedAddress
        self.latitude = place.coordinate.latitude
        self.longitude = place.coordinate.longitude
        self.setMapCenter(lat: self.latitude, long: self.longitude)
    }
    
    func setMapCenter(lat:CLLocationDegrees, long: CLLocationDegrees){
        let allAnnotations = self.mapView.annotations
        for val in allAnnotations{
            self.mapView.removeAnnotation(val)
        }
        
        let annotation = MKPointAnnotation()
        annotation.coordinate = CLLocationCoordinate2D(latitude: lat, longitude: long)
        let center = CLLocationCoordinate2D(latitude: lat, longitude: long)
        let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.005, longitudeDelta: 0.005))
        mapView.setRegion(region, animated: true)
        self.mapView.addAnnotation(annotation)
        CLGeocoder().reverseGeocodeLocation(CLLocation(latitude: lat, longitude: long), preferredLocale: nil) { (response, error) in
            if((response?.count ?? 0) > 0){
                self.address = (response?[0])!
                
            }
        }
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
      
        if !(annotation is MKPointAnnotation) {
            return nil
        }
        let annotationIdentifier = "AnnotationIdentifier"
        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: annotationIdentifier)
        
        if annotationView == nil {
            annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: annotationIdentifier)
            annotationView!.canShowCallout = false
            annotationView?.isDraggable = true
        }
        else {
            annotationView!.annotation = annotation
        }
        
        annotationView!.image = #imageLiteral(resourceName: "map_pin")
        
        
        annotationView?.frame.size =  CGSize(width: 50.0, height: 50.0)
        return annotationView
    }
    

    
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, didChange newState: MKAnnotationView.DragState, fromOldState oldState: MKAnnotationView.DragState) {
             
        if newState == MKAnnotationView.DragState.ending {
                let droppedAt = view.annotation?.coordinate
            self.latitude = droppedAt!.latitude
            self.longitude = droppedAt!.latitude
            self.setMapCenter(lat: droppedAt!.latitude, long: droppedAt!.longitude)
        }
    }
}
