//
//  GalleryViewController.swift
//  Hey Hala
//
//  Created by Sagar Pandit on 11/05/21.
//

import UIKit
import ImageSlideshow
import PINRemoteImage

class GalleryViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, ImageSlideshowDelegate {
   
    func imageSlideshow(_ imageSlideshow: ImageSlideshow, didChangeCurrentPageTo page: Int) {
        let imageSlider = ImageSlideshow()
        let cell = tableView.cellForRow(at: IndexPath(item: page, section: 0)) as! GelleryTableCell
        let fullScreenController = imageSlider.presentFullScreenController(from: self)
       // fullScreenController.view.addSubview(self.addImage(image:self.imageData[page]))
       // fullScreenController.view.sendSubviewToBack(self.addImage(image:self.imageData[page]))
        fullScreenController.slideshow.activityIndicator = DefaultActivityIndicator(style: .white, color: nil)
    }
    
    
    //MARK: IBOutlets
    @IBOutlet weak var tableView: UITableView!
    
    var imageData:[String]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    //MARK:IBAction
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.imageData?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "GelleryTableCell") as! GelleryTableCell
        let val = self.imageData?[indexPath.row]
        let img = UIImageView()
        img.pin_setImage(from: URL(string: U_IMAGE_BASE + (val ?? "")))
        cell.setCustomImage(image:img.image!)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! GelleryTableCell
         let imageSlider = ImageSlideshow()
        
       // imageSlider.addSubview(self.addImage(image:cell.customImageView.image!))
        var input = [SDWebImageSource]()
        for val in self.imageData! {
            input.append(SDWebImageSource(url: URL(string: U_BASE + val)!))
        }
        imageSlider.setImageInputs(input)
        imageSlider.delegate = self
        imageSlider.backgroundColor = .black
  
        let fullScreenController = imageSlider.presentFullScreenController(from: self)
      fullScreenController.slideshow.activityIndicator = DefaultActivityIndicator(style: .white, color: nil)
    }

}

class GelleryTableCell: UITableViewCell {

    @IBOutlet weak var customImageView: UIImageView!

    internal var aspectConstraint : NSLayoutConstraint? {
        didSet {
            if oldValue != nil {
                customImageView.removeConstraint(oldValue!)
            }
            if aspectConstraint != nil {
                customImageView.addConstraint(aspectConstraint!)
            }
        }
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        aspectConstraint = nil
    }

    func setCustomImage(image : UIImage) {

        let aspect = image.size.width / image.size.height

        let constraint = NSLayoutConstraint(item: customImageView, attribute: NSLayoutConstraint.Attribute.width, relatedBy: NSLayoutConstraint.Relation.equal, toItem: customImageView, attribute: NSLayoutConstraint.Attribute.height, multiplier: aspect, constant: 0.0)
        constraint.priority = UILayoutPriority(rawValue: 999)
        aspectConstraint = constraint
        customImageView.image = image
    }
}
