//
//  CategoryViewController.swift
//  Hey Hala
//
//  Created by Apple on 07/04/21.
//

import UIKit
import iOSDropDown
import PINRemoteImage

class CategoryViewController: UIViewController, UITextFieldDelegate, FilterBusiness {
    func refreshBusinessTable(type: Int) {
        self.sortType = type
        self.getCategoryBusiness()
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let  char = string.cString(using: String.Encoding.utf8)!
          let isBackSpace = strcmp(char, "\\b")
        if (isBackSpace == -92 && self.searchField.text!.count == 1) {
            self.filterData = []
            self.storeTable.reloadData()
            
        }else {
        self.filterData = self.storeData.filter{
            (($0.name ?? "").lowercased().contains(((self.searchField.text ?? "") + string)))
        }
        self.storeTable.reloadData()
        }
        return true
    }
    
    //MARK: IBOutlets
    @IBOutlet weak var bgImage: UIImageView!
    @IBOutlet weak var categoryName: DesignableUILabel!
    @IBOutlet weak var categoryDescription: DesignableUILabel!
    @IBOutlet weak var subcategoryOptions: DropDown!
    @IBOutlet weak var subcategoryView: View!
    @IBOutlet weak var storeTable: ContentSizedTableView!
    @IBOutlet weak var noDataLabel: DesignableUILabel!
    @IBOutlet weak var searchField: UITextField!
    
    
    var selectedCategory = GetCategoryResponse()
    var storeData = [GetStoreResponse]()
    var filterData = [GetStoreResponse]()
    var sortType = 1

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.searchField.delegate = self
        self.view.backgroundColor = primaryColor
        self.storeTable.estimatedRowHeight = 120
        self.storeTable.rowHeight = UITableView.automaticDimension
        self.initializeView()
        
        
    }

    func initializeView(){
        self.getCategoryBusiness()
        self.bgImage.pin_setImage(from: URL(string: U_IMAGE_BASE + (self.selectedCategory.image ?? "")))
        self.categoryName.text = self.selectedCategory.category_name
        self.categoryDescription.text = self.selectedCategory.category_type
        if(self.selectedCategory.category_name == "EVENTS" || self.selectedCategory.category_name == "SKILLS AND ARTS"){
            self.subcategoryView.isHidden = false
        }else {
            self.subcategoryView.isHidden = true
        }
    }
    
    func getCategoryBusiness(){
        ActivityIndicator.show(view: self.view)
        let param:[String:Any] = [
            "category": self.selectedCategory.id ?? "",
            "type": self.sortType,
            "longitude": Singleton.shared.userLocation.longitude,
              "latitude": Singleton.shared.userLocation.latitude
        ]
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_GET_CATEGORY_BUSINESS , method: .post, parameter: param, objectClass: GetStores.self, requestCode: U_GET_CATEGORY_BUSINESS) { (response) in
            self.storeData = response.response
            self.storeTable.reloadData()
            ActivityIndicator.hide()
            
        }
    }
    
    //MARK: IBActions
    @IBAction func filterAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "FilterViewController") as! FilterViewController
        myVC.filterDelegate = self
        myVC.sortType = self.sortType
        self.navigationController?.present(myVC, animated: true, completion: nil)
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}

extension CategoryViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(self.storeData.count == 0){
            self.noDataLabel.isHidden = false
        }else {
            self.noDataLabel.isHidden = true
        }
        if(filterData.count > 0){
            return self.filterData.count
        }else {
        return self.storeData.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let val = self.filterData.count > 0 ? self.filterData[indexPath.row]:self.storeData[indexPath.row]
        if(self.selectedCategory.category_name == "EVENTS" || self.selectedCategory.category_name == "SKILLS AND ARTS"){
        let cell = tableView.dequeueReusableCell(withIdentifier: "ItemTablecell2") as! ItemTablecell
            cell.itemImage.pin_setImage(from: URL(string: U_IMAGE_BASE + (val.logo ?? "") ))
            cell.itemName.text = val.name
            cell.itemStatus.text = "\(val.preprationTime ?? 0) min"
            cell.itemRating.text =  "\(val.review?.rating ?? 0)"
            
        return cell
        }else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ItemTablecell") as! ItemTablecell
            cell.itemImage.pin_setImage(from: URL(string: U_IMAGE_BASE + (val.logo ?? "")))
            cell.itemDescription.text = val.description
            cell.itemName.text = val.name
            cell.itemStatus.text = "\(val.preprationTime ?? 0) min"
            cell.itemRating.text =  "\(val.review?.rating ?? 0)"
            cell.distance.text = "\(val.distance ?? "") kms"
            return cell

        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if(self.selectedCategory.category_name == "EVENTS" || self.selectedCategory.category_name == "SKILLS AND ARTS"){
            let myVC = self.storyboard?.instantiateViewController(withIdentifier: "SingleEventViewController") as! SingleEventViewController
            myVC.selectedStore = self.filterData.count > 0 ? self.filterData[indexPath.row]:self.storeData[indexPath.row]
            myVC.isEvent = self.selectedCategory.category_name == "EVENTS" ? true:false
            self.navigationController?.pushViewController(myVC, animated: true)
        }else {
            let myVC = self.storyboard?.instantiateViewController(withIdentifier: "CategoryDetailViewController") as! CategoryDetailViewController
            myVC.selectedStore = self.filterData.count > 0 ? self.filterData[indexPath.row]:self.storeData[indexPath.row]
            self.navigationController?.pushViewController(myVC, animated: true)
        }
    }
    
}
