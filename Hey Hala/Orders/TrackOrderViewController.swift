//
//  TrackOrderViewController.swift
//  Hey Hala
//
//  Created by Apple on 15/04/21.
//

import UIKit
import PINRemoteImage


class TrackOrderViewController: UIViewController {
    //MARK: IBOutlets
    @IBOutlet weak var circleView: UIView!
    @IBOutlet weak var orderTable: ContentSizedTableView!
    
    
    var orderDetail = OrderResponse()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.orderTable.reloadData()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        UIView.performWithoutAnimation {
            self.animate(color:.lightGray, time: 0)
        }
        self.animate(color: .white, time: 2)
    }
    
    func animate(color:UIColor, time:Double)
    {
        let slayer = CAShapeLayer()
        let center = CGPoint(x: (self.circleView.bounds.width / 2), y: (self.circleView.bounds.height/2) + 10)
        let radius: CGFloat = self.circleView.bounds.height/2 - 5
        let startAngle: CGFloat = 4 * .pi / 4
        let endAngle: CGFloat = 0.0
        slayer.path = UIBezierPath(arcCenter: center,
                                   radius: radius,
                                   startAngle: startAngle,
                                   endAngle: endAngle,
                                   clockwise: true).cgPath
        slayer.lineWidth = 5.0
        slayer.lineCap = CAShapeLayerLineCap.round
        slayer.strokeColor = color.cgColor
        slayer.fillColor = UIColor.clear.cgColor
        self.circleView.layer.addSublayer(slayer)
        slayer.strokeEnd = 0.0
        
        let animation = CABasicAnimation(keyPath: "strokeEnd")
        animation.duration = time
        //Customize the time of your animation here.
        
        animation.fromValue = 0.0
        animation.toValue = 1.0
        animation.timingFunction = CAMediaTimingFunction(name:
                                                            CAMediaTimingFunctionName.linear)
        slayer.strokeEnd = 1.0
        slayer.add(animation, forKey: nil)
        self.circleOfDots(color:color)
    }
    
    func circleOfDots(color:UIColor) {
        let shapeLayer = CAShapeLayer()
        let center = CGPoint(x: (self.circleView.bounds.width / 2), y: self.circleView.bounds.height/2 + 10)
        let radius: CGFloat = self.circleView.bounds.height/2 - 5
        let startAngle: CGFloat = 4 * .pi / 4
        let endAngle: CGFloat = 0.0
        shapeLayer.path = UIBezierPath(arcCenter: center,
                                       radius: radius,
                                       startAngle: startAngle,
                                       endAngle: endAngle,
                                       clockwise: true).cgPath
        
        //change the fill color
        shapeLayer.fillColor = UIColor.clear.cgColor
        //you can change the stroke color
        shapeLayer.strokeColor = color.cgColor
        //you can change the line width
        shapeLayer.lineWidth = 25.0
        let one : NSNumber = 1
        let two = Int((shapeLayer.path!.boundingBox.height/1.29))
        
        shapeLayer.lineDashPattern = [one, NSNumber(value: two)]
        shapeLayer.lineCap = CAShapeLayerLineCap.round
        self.circleView.layer.addSublayer(shapeLayer)
    }
    
    //MARK: IBActions
    @IBAction func backAction(_ sender: Any) {
        let transition = CATransition()
        transition.duration = 0.25
        transition.type = .push
        transition.subtype = CATransitionSubtype.fromLeft
        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "TabbarViewController") as! TabbarViewController
        self.navigationController?.view.layer.add(transition, forKey: kCATransition)
        self.navigationController?.pushViewController(myVC, animated: false)
    }
}

extension TrackOrderViewController: UITableViewDelegate ,  UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.orderDetail.orderItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "OrderTableCell") as! OrderTableCell
        let val = self.orderDetail.orderItems[indexPath.row]
        
        cell.orderId.text = self.orderDetail.id
        // cell.orderDate.text = self.orderDetail.date
        cell.itemImage.pin_setImage(from: URL(string: val.product?.image ?? ""))
        cell.itemPrice.text = "SAR \(val.product?.price ?? 0)"
        cell.itemName.text = val.product?.name
        cell.itemDescription.text = val.product?.description
        return cell
    }
}

class OrderTableCell: UITableViewCell {
    //MARK: IBOutlets
    @IBOutlet weak var orderDate: DesignableUILabel!
    @IBOutlet weak var orderId: DesignableUILabel!
    @IBOutlet weak var itemImage: ImageView!
    @IBOutlet weak var itemName: DesignableUILabel!
    @IBOutlet weak var itemDescription: DesignableUILabel!
    @IBOutlet weak var itemPrice: DesignableUILabel!
    @IBOutlet weak var itemRating: DesignableUILabel!
    @IBOutlet weak var deliveryDate: DesignableUILabel!
    @IBOutlet weak var orderQuantity: DesignableUILabel!
    
    var rateButton:(()-> Void)? = nil
    var orderButton:(()-> Void)? = nil
    
    //MARK: IBActions
    @IBAction func rateAction(_ sender: Any) {
        if let rateButton = self.rateButton {
            rateButton()
        }
    }
    
    @IBAction func repeatOrderAction(_ sender: Any) {
        if let orderButton = self.orderButton {
            orderButton()
        }
    }
    
}
