//
//  OrderDetailViewController.swift
//  Hey Hala
//
//  Created by Apple on 14/04/21.
//

import UIKit
import PINRemoteImage

class OrderDetailViewController: UIViewController {
    //MARK: IBOutlets
    @IBOutlet weak var itemImage: ImageView!
    @IBOutlet weak var itemName: DesignableUILabel!
    @IBOutlet weak var distance: DesignableUILabel!
    @IBOutlet weak var ratingLabel: DesignableUILabel!
    @IBOutlet weak var itemDescription: DesignableUILabel!
    @IBOutlet weak var orderId: DesignableUILabel!
    @IBOutlet weak var orderDate: DesignableUILabel!
    @IBOutlet weak var delvieryDate: DesignableUILabel!
    @IBOutlet weak var orderTable: ContentSizedTableView!
    @IBOutlet weak var taxPrice: DesignableUILabel!
    @IBOutlet weak var itemTotal: DesignableUILabel!
    @IBOutlet weak var taxPercent: DesignableUILabel!
    @IBOutlet weak var orderTotal: DesignableUILabel!
    @IBOutlet weak var address: DesignableUILabel!
    
    var orderData = OrderResponse()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initView()
    }
    
    func initView(){
        self.itemImage.pin_setImage(from: URL(string: self.orderData.orderItems[0].business?.logo ?? ""))
        self.itemDescription.text = self.orderData.orderItems[0].business?.description
        self.itemName.text = self.orderData.orderItems[0].business?.name
        self.distance.text = self.orderData.orderItems[0].business?.location
        self.ratingLabel.text = "\(self.orderData.orderItems[0].business?.review?.rating ?? 0)"
        self.orderId.text = "Order Id - \(self.orderData.order_id ?? 0)"
        self.orderDate.text = String((self.orderData.created_at ?? "").prefix(10))
        self.delvieryDate.text = String((self.orderData.created_at ?? "").prefix(10))
        self.itemTotal.text = "SAR\(self.orderData.total ?? 0)"
        self.orderTotal.text = "SAR\(self.orderData.total ?? 0)"
        self.address.text = self.orderData.address
    }
    
    //MARK: IBActions
    @IBAction func trackOrderAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "TrackOrderViewController") as! TrackOrderViewController
        myVC.orderDetail = self.orderData
        self.navigationController?.pushViewController(myVC, animated: true)
    }
    
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}

extension OrderDetailViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.orderData.orderItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "OrderTableCell") as! OrderTableCell
        let val = self.orderData.orderItems[indexPath.row]
        cell.itemName.text = val.product?.name
        cell.itemPrice.text = "\(val.quantity ?? 0)xSAR\(val.price ?? 0)"
        cell.itemDescription.text = "SAR\(val.price ?? 0)"
        return cell
    }
    
    
}
