//
//  OrderNowViewController.swift
//  Hey Hala
//
//  Created by Apple on 14/04/21.
//

import UIKit

class OrderNowViewController: UIViewController {
    //MARK: IBOutlets
    @IBOutlet weak var bgImage: UIImageView!
    @IBOutlet weak var productName: DesignableUILabel!
    @IBOutlet weak var productDescription: DesignableUILabel!
    @IBOutlet weak var heartImage: UIImageView!
    @IBOutlet weak var reviewRating: DesignableUILabel!
    @IBOutlet weak var statusLabel: DesignableUILabel!
    @IBOutlet weak var itemPrice: DesignableUILabel!
    @IBOutlet weak var reviewLabel: DesignableUILabel!
    @IBOutlet weak var ingredientDescription: DesignableUILabel!
    
    var selectedProduct = GetStoreResponse()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.initializeView()
    }
    
    
    func initializeView(){
        self.checkProductFavourite()
        self.bgImage.pin_setImage(from: URL(string: U_IMAGE_BASE + (self.selectedProduct.image ?? "")))
        self.productName.text = self.selectedProduct.name
        self.productDescription.text = self.selectedProduct.description
        self.reviewRating.text = "\(self.selectedProduct.review?.rating ?? 0)"
        self.reviewLabel.text = "\(self.selectedProduct.review?.reviews ?? 0) reviews"
        self.itemPrice.text = "SAR \(self.selectedProduct.price ?? 0).00"
        self.statusLabel.text = "\(self.selectedProduct.preprationTime ?? 0) minutes"
        self.ingredientDescription.text = self.selectedProduct.ingredients ?? ""
    }
    
    
    func checkProductFavourite(){
        ActivityIndicator.show(view: self.view)
        
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_CHECK_FAVOURITE_PRODUCT + "\(self.selectedProduct.id ?? "")" , method: .get, parameter: nil, objectClass: CheckFavourite.self, requestCode: U_CHECK_FAVOURITE_PRODUCT) { (response) in
            ActivityIndicator.hide()
            if(response.response?.is_favourite == 1){
                self.heartImage.image = #imageLiteral(resourceName: "like")
            }else {
                self.heartImage.image = #imageLiteral(resourceName: "heart_outline")
            }
        }
    }
    
    
    //MARK: IBActions
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)

    }
    
    @IBAction func reviewAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "RateReviewViewController") as! RateReviewViewController
        myVC.produtDetail = self.selectedProduct
        self.navigationController?.pushViewController(myVC, animated: true)
    }
    
    @IBAction func orderNowAction(_ sender: Any) {
        ActivityIndicator.show(view: self.view)
        let param:[String:Any] = [
            "product": self.selectedProduct.id ?? "",
            "quantity": 1,
            "business":self.selectedProduct.business ?? "",
        ]
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_ADD_TO_CART, method: .post, parameter: param, objectClass: SuccessResponse.self, requestCode: U_ADD_TO_CART) { (response) in
            ActivityIndicator.hide()
            Singleton.shared.cartData = GetCartResponse()
            self.showMsg(controller: self, text: "Successfully added to cart")
            let myVC = self.storyboard?.instantiateViewController(withIdentifier: "MyCartViewController") as! MyCartViewController
            myVC.showBackButton = true
            self.navigationController?.pushViewController(myVC, animated: true)
        }
    }
    
    
    @IBAction func heartAction(_ sender: Any) {
        var url = String()
        if(heartImage.image == #imageLiteral(resourceName: "heart_outline")){
            self.heartImage.image = #imageLiteral(resourceName: "like")
            url = U_BASE + U_MARK_PRODUCT_FAVOURITE
        }else {
            self.heartImage.image = #imageLiteral(resourceName: "heart_outline")
            url = U_BASE + U_REMOVE_FAVOURITE_PRODUCT
        }
        
        SessionManager.shared.methodForApiCalling(url: url , method:.post, parameter: ["product":self.selectedProduct.id ?? ""], objectClass: CheckFavourite.self, requestCode: url) { (response) in
            if(self.heartImage.image != #imageLiteral(resourceName: "heart_outline")){
                self.showMsg(controller: self, text: "Successfully marked as favourite")
            }else {
                self.showMsg(controller: self, text: "Removed from favourite")
            }
            ActivityIndicator.hide()
        }
    }
    
}
