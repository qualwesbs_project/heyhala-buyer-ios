//
//  CheckoutViewController.swift
//  Hey Hala
//
//  Created by Apple on 20/04/21.
//

import UIKit

class CheckoutViewController: UIViewController, SlideButtonDelegate {
    
    func buttonStatus(status: String, sender: MMSlidingButton) {
        if(status == "Unlocked"){
            self.callConfirmOrder()
        }
    }

    //MARK: IBOutlets
    @IBOutlet weak var slidingButton: MMSlidingButton!
    @IBOutlet weak var itemTotal: DesignableUILabel!
    @IBOutlet weak var taxRate: DesignableUILabel!
    @IBOutlet weak var deliveryFee: DesignableUILabel!
    @IBOutlet weak var cartTotal: DesignableUILabel!
    @IBOutlet weak var streetAddress: DesignableUILabel!
    @IBOutlet weak var address: DesignableUILabel!
    
    var selectedAddress = AddressResponse()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.slidingButton.delegate = self
        self.address.text = (selectedAddress.location ?? "") + ", " + (selectedAddress.city ?? "")
        self.address.text = (self.address.text ?? "")  + ", " + (selectedAddress.state ?? "")
        
        self.streetAddress.text = (selectedAddress.house ?? "") + " " + (selectedAddress.street ?? "")

        NavigationController.shared.getcartData(view: self) { (response) in
            
            self.itemTotal.text = "SAR \(response.total ?? 0)"
            self.taxRate.text = "SAR \(response.tax_rate ?? 0)"
            self.deliveryFee.text = "SAR 0"
            self.cartTotal.text = "SAR \(response.total ?? 0)"
        }
    }
    
    func callConfirmOrder(){
        ActivityIndicator.show(view: self.view)
        
        let param:[String:Any] = [
            "address": self.selectedAddress.id ?? "",
            "delivery_type": 1
        ]
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_PLACE_ORDER, method: .post, parameter:param, objectClass: PlaceOrderResponse.self, requestCode: U_PLACE_ORDER) { (response) in
            ActivityIndicator.hide()
            Singleton.shared.cartData = GetCartResponse()
            let myVC = self.storyboard?.instantiateViewController(withIdentifier: "OrderConfirmationViewController") as! OrderConfirmationViewController
            myVC.orderDetail = response.response
            self.navigationController?.pushViewController(myVC, animated: true)
        }
    }
    
    //MARK: IBActions
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
