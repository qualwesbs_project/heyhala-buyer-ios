//
//  RateReviewViewController.swift
//  Hey Hala
//
//  Created by Sagar Pandit on 28/06/21.
//

import UIKit

class RateReviewViewController: UIViewController {
    //MARK: IBOutlets
    @IBOutlet weak var reviewTable: UITableView!
    @IBOutlet weak var noReviewLabel: DesignableUILabel!
    
    var produtDetail = GetStoreResponse()
    var reviewData = RateReviewResponse()
    

    override func viewDidLoad() {
        super.viewDidLoad()
        getReviewData()
    }
    
    func getReviewData(){
        ActivityIndicator.show(view: self.view)
        var url = ""
        self.noReviewLabel.isHidden = false
        if(self.produtDetail.business != nil){
            url = U_BASE + U_GET_PRODUCT_REVIEW + "\(self.produtDetail.id ?? "")"
        }else{
            url = U_BASE + U_GET_BUSINESS_REVIEW + "\(self.produtDetail.id ?? "")"
        }
        SessionManager.shared.methodForApiCalling(url: url, method: .get, parameter: nil, objectClass: GetRateReviews.self, requestCode: url) { response in
            self.reviewData = response.response
            self.reviewTable.reloadData()
            ActivityIndicator.hide()
        }
    }
    
    //MARK: IBActions
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func addReviewAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "SingleArtViewController") as! SingleArtViewController
        myVC.selectedArt = self.produtDetail
        myVC.isReviewProduct = true
        self.navigationController?.pushViewController(myVC, animated: true)
    }
}

extension RateReviewViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(self.reviewData.reviews.count == 0){
            self.noReviewLabel.isHidden = false
        }else {
            self.noReviewLabel.isHidden = true
        }
        return self.reviewData.reviews.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ItemTablecell") as! ItemTablecell
        let val = self.reviewData.reviews[indexPath.row]
        cell.itemName.text = val.fullName
        cell.distance.text = "\((val.created_at ?? "").prefix(10))"
        cell.ratingView.rating = Double(val.score ?? "0")!
        cell.itemRating.text = val.description
        return cell
    }
    
    
}
